source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'mailboxer'

# trailblazer stack
gem 'trailblazer-rails'
gem 'trailblazer-cells'
gem 'cells-slim'
gem 'cells-rails'
gem "dry-validation"

gem 'puma'
gem 'slim-rails'
gem 'foundation-rails'

gem 'stamp', '0.4.0' # does not work with newer versions
gem 'stamp-i18n'


gem "bower-rails", "~> 0.11.0"

gem 'chronic'
gem 'i18n'
gem 'rails-i18n', '~> 4.0.0' # For 4.0.x
gem 'roo', '~> 2.4.0'
gem 'jquery-ui-rails'
gem 'rubocop', '~> 0.44.1', require: false
gem 'acts-as-taggable-on', '~> 4.0'

gem 'fog-aws', git: 'https://github.com/rubentrf/fog-aws', branch: 'get_identities_verification'

gem 'fog'
gem 'carrierwave', '>= 1.0.0.beta', '< 2.0'
gem 'mini_magick'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'semantic-ui-sass', git: 'https://github.com/doabit/semantic-ui-sass'
gem 'rails-erd'
gem 'pundit'
gem 'yt', '~> 0.28.0'
gem 'd3-rails'
gem 'storext'
gem 'sidekiq'
gem 'rest-client'

group :development, :test do
  gem 'dotenv-rails'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'minitest-rails'
  gem 'fabrication'
  gem 'pry-byebug'
end


group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'better_errors'
  gem 'annotate', '~> 2.6.5'
  gem 'quiet_assets'
end
