Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'  
  
  concern :messageable do
    collection do
      get :inbox
      get :sent
      get :trash
    end
    post :reply, on: :member
  end

  resources :rooms
  resources :comments
  resources :statistics
  resources :preferences
  
  namespace :admin do
    resources :users do
      resources :parents do 
        post :add, on: :collection
        get :remove, on: :member
      end
      get :search, on: :collection
      post :avatar, on: :member
    end
    resources :schedules do
      post :update, on: :member
    end
    resources :rooms do
      resources :users, controller: 'rooms/users' do
        post :add, on: :collection
        get :remove, on: :member
      end
    end
  end

  namespace :teacher do
    resources :students
    resources :announcements
    resources :messages do
      collection do
        get :inbox
        get :sent
        get :trash
      end
      post :reply, on: :member
    end
    resources :courses, shallow: true do
      post :avatar, on: :member

      resources :options do
        post :avatar, on: :member
      end
      
      resources :videos      
      resources :homeworks do 
        resources :questions do
          post :avatar, on: :member
        end

        scope module: 'homeworks' do    
          resources :tools do
            collection do 
              get :import_html
              post :import
            end
          end
        end
      end
    end  
  end

  namespace :student do
    resources :homeworks do
      get :waiting, on: :collection
      get :expired, on: :collection
      resources :takes
    end
    resources :rooms do
      get :schedule, on: :member
    end
    resources :messages, concerns: :messageable
    resources :answers
    resources :announcements
  end

  namespace :parent do
    resources :messages, concerns: :messageable
  end

  get '/admin/users/tools/import_html' => "admin/users/tools#import_html"
  post '/admin/users/tools/import' => "admin/users/tools#import", as: 'import_users'

  # Home page per role
  get 'admin' => 'admin/users#index'
  get 'teacher' => 'teacher/courses#index'
  get 'student' => 'student/homeworks#index'
  get 'parent' => 'parent/messages#index'
  root 'student/homeworks#index'



  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
