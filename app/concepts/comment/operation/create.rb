class Comment::Create < Trailblazer::Operation
  extend Contract::DSL
  contract Announcement::Contract::Create

	step 	Model( Comment, :new)
  step  :assign_current_user!
  step  :assign_commentable!
  step  Contract::Build()
  step  Contract::Validate()
  step  Contract::Persist()


  def assign_commentable!(options, params:, **)
  	type, id = params.fetch(:commentable_type),
  						 params.fetch(:commentable_id)
    options["model"].commentable = type.constantize.find_by(id)
  end

  def assign_current_user!(options)
  	options["model"].user =  options["current_user"]
  end    
 end
