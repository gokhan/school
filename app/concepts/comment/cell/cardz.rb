module Comment::Cell
  class Cardz < Trailblazer::Cell
    property :body
    property :user
    property :updated_at

    def comment
      model
    end

  end
end
