module Comment::Contract
  require "reform/form/dry"

  class Create < Reform::Form
    model Course

    feature Reform::Form::Dry

    property :user
    property :body
    property :commentable_id
    property :commentable_type


    validation :default do
      required(:user).filled
      required(:body).filled
      required(:commentable_type).filled
      required(:commentable_id).filled
    end
  end

end
