class Video::Create < Trailblazer::Operation
  extend Contract::DSL
  contract Video::Contract::Create

  step  Model( Video, :new )
  step  Policy::Pundit( VideoPolicy, :create? )
  step  :assign_current_user!
  step  :assign_videoable!
  step  Contract::Build()
  step  Contract::Validate()
  step  Contract::Persist()

  def assign_current_user!(options)
    options["model"].user =  options["current_user"]
  end

  def assign_videoable!(params)
    options["model"].videoable =  Course.find(params.fetch(:course_id))
  end

  #TODO: how to do in TR2
  # def setup_params!(params)
  #   params[:publich_at] = Chronic::parse(params[:publish_at]) if params[:publish_at]
  # end

end