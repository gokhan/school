class Video::Show < Trailblazer::Operation
	step  Model( Video, :find_by)	
	step  Policy::Pundit( VideoPolicy, :show? )
end