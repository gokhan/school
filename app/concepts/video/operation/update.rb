class Video::Update < Video::Create
	step  Model( Video, :find_by ), override: true
	step  Policy::Pundit( VideoPolicy, :update? ), override: true
end