  class Video::List < Trailblazer::Operation
  	step  :model!
  	step  Policy::Pundit( VideoPolicy, :list? )

    def model!(**)
      options["model"] = Video.all.order(publish_at: :desc)
    end
  end