module Video::Contract
  require "reform/form/dry"

  class Create < Reform::Form
    model Video

    feature Reform::Form::Dry

    property :user
    property :videoable
    property :link
    property :title
    property :description
    property :publish_at,  prepopulator: :publish_at!

    def publish_at!(options)
      return if model.publish_at.blank?
      self.publish_at = model.publish_at.strftime("%Y-%m-%d %H:%M") 
    end

    validation :default do
        configure do
          option :form
          config.messages_file = 'config/locales/error_messages.yml'

          def youtube_url?(value)
            begin 
              yt = Yt::Video.new( url: value )
              yt.title
            rescue
              return false
           end

           true
          end
        end

      required(:user).filled
      required(:videoable).filled
      required(:link).filled(:youtube_url?)
      required(:title).filled
      required(:description).filled      
      required(:publish_at).filled
    end
  end

  class Update < Create
  end
end
