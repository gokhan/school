module Homework::Cell
  class QuestionsImport < Trailblazer::Cell
    include School::Cell::Helpers

    def homework
      context[:homework]
    end

    def url
      options[:url]
    end

    private
    property :contract
  end
end
