module Video::Cell
  class Create < Trailblazer::Cell
    include School::Cell::Helpers

    def videoable
      @model.model.videoable
    end

    def url
      options[:url]
    end

		private
		property :contract
  end
end
