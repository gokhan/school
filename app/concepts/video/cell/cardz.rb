module Video::Cell
  class Cardz < Trailblazer::Cell
    property :link
    property :title
    property :description
    property :user
    property :publish_at

    def video
      model
    end

    def embed_video
      yt.embed_html 
    end
    
    def edit_url
      "/teacher/videos/#{video.id}/edit"
    end

    def yt
      @yt ||= Yt::Video.new( url: video.link )
    end
  end
end
