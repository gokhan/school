class Room::Show < Trailblazer::Operation
	step  Model( Room, :find_by )
	step  Policy::Pundit( RoomPolicy, :show? )
end