class Room::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation
	  step  Model( Room, :new )
	  step  Policy::Pundit( RoomPolicy, :create? )
	  step  Contract::Build( constant: Room::Contract::Create )
	end	
	step  	Nested( Present )
  step  	Contract::Validate(key: "room")
  failure Contract::Persist(method: :sync)
  step  	Contract::Persist()
end

# class Create < Trailblazer::Operation
#   include Model
#   model Room, :create

#   include Trailblazer::Operation::Policy
#   policy RoomPolicy, :create?

#   contract do
#     feature Reform::Form::Dry

#     property :name

#     validation :default do
#       required(:name).filled 
#     end
#   end 

#   def process(params)
#     validate(params[:room]) do
#       contract.save
#     end
#   end
# end