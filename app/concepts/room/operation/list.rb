class Room::List < Trailblazer::Operation
	step  :model!
	step  Policy::Pundit( RoomPolicy, :list? )	
	
	def model!(options)
		options["model"] = Room.all
	end
end