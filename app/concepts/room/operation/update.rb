class Room::Update < Trailblazer::Operation
	class Present < Trailblazer::Operation
	  step  Model( Room, :find_by)
	  step  Policy::Pundit( RoomPolicy, :create? )
	  step  Contract::Build( constant: Room::Contract::Create )
	end	
	
	step  	Nested( Present )	
	step 		Contract::Validate(key: "room")
	failure Contract::Persist(method: :sync)
	step 		Contract::Persist()
end