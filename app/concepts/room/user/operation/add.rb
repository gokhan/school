class Room < ActiveRecord::Base
	module User
		class Add < Trailblazer::Operation
			step  :process!

			def process!(options, params:, **)
		    ::UserRoom.where(room_id: params.fetch(:room_id),
		                     user_id: user_id(params)
		                   ).first_or_create
			end

		  def user_id(params)        
		    params.fetch(:user_id, nil) || ::User.find_by(email: params.fetch(:email)).id
		  end
		end
	end
end