class Room < ActiveRecord::Base
	module User
		class Remove < Trailblazer::Operation
			step	:process!

		  def process!(options, params:, **)
		    UserRoom.find_by(room_id: params.fetch(:room_id),
		                     user_id: params.fetch(:id)
		                    ).destroy
		  end
		end
	end
end