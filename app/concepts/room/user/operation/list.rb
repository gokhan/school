class Room < ActiveRecord::Base
	module User
		class List < Trailblazer::Operation
			step	:model!
			step  Policy::Pundit( RoomPolicy, :list? )

			def model!(options, params:, **)
				options["model"] = OpenStruct.new(users: room(params).users, room: room(params))
			end

			def room(params)
		    Room.find(params.fetch(:room_id))
		  end
		end
	end
end