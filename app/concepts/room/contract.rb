module Room::Contract
  class Create < Reform::Form
    property :name

    validation :default do
      required(:name).filled
    end
  end
end
