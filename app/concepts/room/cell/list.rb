module Room::Cell
  class List < Trailblazer::Cell
    include School::Cell::Helpers 

    def collection
      @model.model
    end

    def edit_url(room)
      "/admin/rooms/#{room.id}/edit"
    end

    def students_url(room)
      "/admin/rooms/#{room.id}/users"
    end
    
    def course_url(course)
      "/teacher/courses/#{course.id}"
    end

    def number_of_students(record)
      record.users.count
    end

    def active?(record)
      record.active? ? "evet" : "hayir"
    end
  end
end
