module Room::Cell
  class Show < Trailblazer::Cell
    include School::Cell::Helpers

    def homeworks
      model.homeworks
    end

    def events
      ( model.homeworks | model.announcements | model.comments ).sort_by { |r| r[:updated_at] }.reverse
    end

    def courses
      model.courses
    end

    def students
      model.users
    end

    def comments
      model.comments
    end
    
    def room
      model
    end

    protected
    property :name
  end
end
