module Room::Cell
  class Users < Trailblazer::Cell

    def title
      room.name
    end

    def remove_url(record)
      "/admin/rooms/#{room.id}/users/#{record.id}/remove"
    end

    def avatar_url(record)
      record.avatar.data.square.url
    end  
    
    def role(record)
      record.role.name
    end

    def room
      model.room
    end

    def users
      model.users
    end
  end
end
