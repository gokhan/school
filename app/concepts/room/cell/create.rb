module Room::Cell
  class Create < Trailblazer::Cell
    include School::Cell::Helpers
    
    def title
      'Sinif ' + ( model.persisted? ? "guncelleme (#{model.name})" : 'ekleme' )
    end
  end
end
