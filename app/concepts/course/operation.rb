# require "reform/form/dry"

# class Course < ActiveRecord::Base
#   class Create < Trailblazer::Operation
#     include Model
#     model Course, :create

#     include Trailblazer::Operation::Policy
#     policy CoursePolicy, :create?


#     contract Contract::Create

#     def process(params)
#       validate(params[:course]) do
#         contract.save
#       end
#     end

#     def setup_model!(params)
#       model.teacher = params.fetch(:current_user)
#     end
#   end

#   class Update < Create
#     include Trailblazer::Operation::Policy
#     policy CoursePolicy, :update?

#     action :update
#   end

#   class List < Trailblazer::Operation
#     include Trailblazer::Operation::Policy
#     policy CoursePolicy, :list?

#     include Collection

#     def model!(params)
#       Course.all
#     end
#   end

#   class Homeworks < Trailblazer::Operation
#     include Trailblazer::Operation::Policy
#     policy CoursePolicy, :homeworks?

#     include Collection

#     def model!(params)
#       Course.find(params[:id]).homeworks
#     end
#   end

#   class Show < Trailblazer::Operation
#     include Model
#     model Course, :find

#     include Trailblazer::Operation::Policy
#     policy CoursePolicy, :show?

#   end
# end
