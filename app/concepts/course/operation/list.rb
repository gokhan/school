class Course::List < Trailblazer::Operation
	step :model!
  step Policy::Pundit( CoursePolicy, :list? )

  def model!(options)
  	options["model"] = Course.all
  end
end
