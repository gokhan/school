  class Course::Create < Trailblazer::Operation
	  class Present < Trailblazer::Operation
	    step  Model( Course, :new )
	    step  Policy::Pundit( CoursePolicy, :create? )
	    step  :assign_current_user!
	    step  Contract::Build( constant: Course::Contract::Create )
	    
	    def assign_current_user!(options)
	      options["model"].user = options["current_user"]
	    end
	  end  	

    step  Nested( Present )
    step  Contract::Validate(key: "course")
    failure Contract::Persist(method: :sync)
    step  Contract::Persist()
  end