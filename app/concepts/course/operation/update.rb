class Course::Update < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step  Model( Course, :find_by )
    step  Policy::Pundit( CoursePolicy, :create? )
    step  Contract::Build( constant: Course::Contract::Create )
  end	
	step  	Nested( Present )
	step  	Contract::Validate(key: "course")
	failure Contract::Persist(method: :sync)
	step  	Contract::Persist()
end