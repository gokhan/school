class Course::Homeworks < Trailblazer::Operation
	step 	:model!
  step 	Policy::Pundit( CoursePolicy, :homeworks? )

  def model!(options, params:, **)
    options["model"] = Course.find(params[:id]).homeworks
  end
end