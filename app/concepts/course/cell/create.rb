module Course::Cell
  class Create < Trailblazer::Cell
    include School::Cell::Helpers

    def url
      options[:url]
    end

		private
		property :contract
  end
end
