module Course::Cell
  class Show < Trailblazer::Cell
    property :name
    property :description
    property :teacher

    def class_name(index)
      index.even? ? 'direction-l' : 'direction-r'
    end

    def events
      (model.homeworks | model.videos).sort_by{|model| model["updated_at"]}
    end
  end
end
