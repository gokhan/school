module Course::Cell
  class Title < Trailblazer::Cell
    property :id
    property :name
    property :description
  end
end
