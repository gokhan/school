module Course::Cell
  class Card < Trailblazer::Cell
    property :name
    property :description
    property :teacher

    def image
      ["http://static.pexels.com/wp-content/uploads/2014/07/alone-clouds-hills-1909-527x350.jpg"].sample
    end

    def homework_count
      model.homeworks.count
    end

    def student_count
      model.room.users.count
    end

    def url
      "/teacher/courses/#{model.id}"
    end
  end
end
