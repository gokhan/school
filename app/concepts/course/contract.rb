require "reform/form/dry"

module Course::Contract
  class Create < Reform::Form
    feature Reform::Form::Dry

    property :user
    property :name
    property :description
    property :lesson_id
    collection :subject_ids
    property :room_id

    #TODO: we need to move these image items into a library to include
    property :image, populate_if_empty: :populate_image! do
      property :data
    end


    validation :default do
      required(:user).filled
      required(:room_id).filled
      required(:description).filled
      required(:lesson_id).filled
    end

    def populate_image!(options) 
      model.image || Image.new
    end
  end
end
