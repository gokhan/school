require 'representable/json'

class CourseRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :avatar do
    property :data do
      property :thumb
      property :square
    end
  end
end 