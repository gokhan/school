class Take::FindOrCreate < Trailblazer::Operation
	success	:model!
  step  :process!
  def process!(options, params:, **)
    Take::UpdateResults.( take_id: options["model"].id )
  end

  def model!(options, params:, **) 
    options["model"] = Take.find_or_create_by(homework_id: params[:homework_id], 
    																					user: options["current_user"])
  end
end
