class Take::Questions < Trailblazer::Operation
	step  :model!

  def model!(options, params:, **)
    options["model"] = Take.find(params[:id])
    											 .homework.questions
    										   .includes(:choices)
    										   .order(:created_at)
  end
end