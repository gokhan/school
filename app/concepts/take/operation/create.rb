class Take::Create < Trailblazer::Operation
	extend Contract::DSL
  contract Take::Contract::Create

  step  Model( Take, :new )
  step  :assign_current_user!
  step  :assign_homework!
  step  Contract::Build()
  step  Contract::Validate()
  step  Contract::Persist()

  def assign_current_user!(options, params:, **)
  	options["model"].user =  options["current_user"]
  end

  def assign_homework!(options, params:, **)
  	options["model"].homework = Homework.find(params[:homework_id])
  end
end