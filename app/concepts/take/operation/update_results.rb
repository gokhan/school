class Take::UpdateResults < Trailblazer::Operation
  step  :process!
  def process!(options, params:, **)
    take = Take.find_by(id: params.fetch(:take_id))

    right = blank = wrong = 0
    take.answers.includes(:question).each do |answer|
      if answer.question.correct == answer.option
        right += 1 
      else
        wrong += 1
      end
    end
    blank = take.homework.questions.count - take.answers.count

    take.update_attributes(right: right, wrong: wrong, blank: blank)
  end
end
