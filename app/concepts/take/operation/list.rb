class Take::List < Trailblazer::Operation
	step  :model!

  def model!(options, params:, **)
	  options["model"] = Take.order(updated_at: :desc).all
	end
end
