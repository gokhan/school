module Homework::Cell
  class List < Trailblazer::Cell
    def deadline(homework)
      homework.ends_at.localize_stamp("January 10 20:20")
    end

    def class_name(index)
      index.even? ? 'direction-l' : 'direction-r'
    end

    def take_url(homework)
      "/student/homeworks/#{homework.id}/takes/new"
    end
    
    def collection
      @model.model
    end
  end
end
