module Homework::Cell
  class Show < Trailblazer::Cell
    property :name
    property :description
    property :user
  end
end
