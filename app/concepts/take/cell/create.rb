module Take::Cell
  class Create < Trailblazer::Cell
    include School::Cell::Helpers

    def course
      @model.model.course
    end

    def url
      options[:url]
    end

		private
		property :contract
  end
end
