module Take::Cell
  class Questions < Trailblazer::Cell

    def homework
      context[:homework]
    end

    def take
      context[:take]
    end
    
    def collection
      @model.model
    end

    def checked(take, question, choice)
      ::Answer.find_by(user: context[:current_user], take: take, question: question, option: choice).present? ? 'checked' : ''
    end
  end
end
