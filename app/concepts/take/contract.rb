module Take::Contract
  require "reform/form/dry"

  class Create < Reform::Form
    model Take

    feature Reform::Form::Dry

    property :user
    property :homework
   
    validation :default do
      required(:user).filled
      required(:homework).filled   
    end
  end

  class Update < Create
  end
end
