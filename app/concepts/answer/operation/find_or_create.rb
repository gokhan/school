class Answer::FindOrCreate < Trailblazer::Operation
	step :model!

  def process(params)
    self["model"] = unless self["model"]
					               Create.call(params)["model"]
					             else
					               Update.call(params.merge(id: self["model"].id))["model"]
					             end
    Take::UpdateResults.(take_id: params.fetch(:answer).fetch(:take_id) )
  end

  def model!(options, params:, **) 
    options["model"] = Answer.find_by( take_id: params.fetch(:answer).fetch(:take_id), 
                    question_id: params.fetch(:answer).fetch(:question_id), 
                    user: options["current_user"] )
  end
end