class Answer::Create < Trailblazer::Operation
	extend Contract::DSL
  contract Answer::Contract::Create

  step  Model( Answer, :new )
  step  :assign_current_user
  step  Contract::Build()
  step  Contract::Validate()
  step  Contract::Persist()

  def assign_current_user!(options)
  	options["model"].user =  options["current_user"]
  end
end