module Answer::Contract
  require "reform/form/dry"

  class Create < Reform::Form
    model Answer

    feature Reform::Form::Dry

    property :take_id
    property :question_id
    property :option_id
  end

  class Update < Create
  end
end
