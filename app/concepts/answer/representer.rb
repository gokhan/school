require 'representable/json'

class AnswerRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :question_id
  property :option_id
  property :take do
    property :id
    property :right
    property :wrong
    property :blank
  end

end 