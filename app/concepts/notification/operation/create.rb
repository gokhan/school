class Notification::Create < Trailblazer::Operation
  extend Contract::DSL
  contract do
    property :sourceable
    property :action

    validation :default do
      required(:sourceable).filled
      required(:action).filled
    end
  end

  step  Model( Notification, :new )
  step  :assign_sourceable!
  step  Contract::Build()
  step  Contract::Validate()
  step  Contract::Persist()

  def assign_sourceable!(options, params:, **)
    options["model"].sourceable =  params["sourceable"]
  end
end