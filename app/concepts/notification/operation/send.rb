class Notification::Send < Trailblazer::Operation
  step :process!

  def process!(options, params:, **)
    api_url = "https://api:#{ENV['MAILGUN_API_KEY']}@api.mailgun.net/v3/#{ENV['MAILGUN_DOMAIN']}/messages"

    mail_params = { 
                :from => "postmaster@#{ENV['MAILGUN_DOMAIN']}",
                :to => "gokhan@sylow.net",
                :subject => "Yeni odeviniz var",
                :text => "Text body",
                :html => "<b>HTML</b> version of the body!"
              }
   begin
     RestClient.post api_url, mail_params
   rescue RestClient::ExceptionWithResponse => e
     puts e.response
   end
  end
end