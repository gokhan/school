module Student 	
	class Message::List < Trailblazer::Operation
		step  :model!

		def model!(options, params:, **)
		  options["model"] = options["current_user"].messages
		end
	end
end