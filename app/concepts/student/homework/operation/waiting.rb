class Student::Homework::Waiting < Trailblazer::Operation
  step  :model!
  step  Policy::Pundit( StudentPolicy, :homeworks? )

  def model!(options, params:, **)
    options["model"] = all(options, params).select{ |homework| homework.ends_at >= DateTime.now }
  end

  def all(options, params)
    courses(options, params).collect{ |course| course.homeworks }.flatten.uniq
  end

  def courses(options, params)
    options["current_user"].rooms.collect{ |room| room.courses }.flatten.uniq
  end
end