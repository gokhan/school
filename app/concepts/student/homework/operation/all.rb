class Student::Homework::All < Trailblazer::Operation
  step  :model!
  step  Policy::Pundit( StudentPolicy, :homeworks? )

  def model!(options, params:, **)
    options["model"] = courses(options, params).collect{ |course| course.homeworks }.flatten.uniq
  end

  def courses(options, params)
    options["current_user"].rooms.collect{ |room| room.courses }.flatten.uniq
  end
end