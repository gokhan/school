module Message::Contract
  class Create < Reform::Form

    property :user
    property :subject
    property :body
    property :to
    property :rooms
    property :parents

    validation :default do
      required(:subject).filled
      required(:body).filled
    end

  end

  class Reply < Reform::Form
    property :user
    property :body

    validation :default do
      required(:body).filled
    end
  end
end
