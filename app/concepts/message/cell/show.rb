module Message::Cell
  class Show < Trailblazer::Cell
    include School::Cell::Helpers

    def receipts
      model.receipts_for context[:current_user]
    end

    def subject
      model.subject
    end

    def body(record)
      record.body
    end

    def created_at(record)
      record.created_at.localize_stamp("20:20, January 10")
    end

    
    def reply_url
      options[:url]
    end

    def avatar_url(record)
      record.avatar.data.square.url
    end

    def contract
      options[:operation].contract
    end

  end
end
