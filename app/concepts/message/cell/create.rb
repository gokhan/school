module Message::Cell
  class Create < Trailblazer::Cell
    include School::Cell::Helpers

    def url
      options[:url]
    end

    def teachers
      ::User.teachers.order(:name)
    end

    def users
      ::User.order(:name)
    end

    def rooms
      ::Room.order(:name)
    end

    def teacher?
      context[:current_user].is_teacher?
    end
    
    def parent?
      context[:current_user].is_parent?
    end

    def student?
      context[:current_user].is_student?
    end
  end
end
