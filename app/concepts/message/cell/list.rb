module Message::Cell
  class List < Trailblazer::Cell


    def participants(conversation)
      conversation.participants.collect(&:name).join(", ")
    end

    def class_name(record)
      record.is_unread?(context[:current_user]) ? 'unread' : 'read'
    end

    def subject(record)
      record.subject
    end

    def detail_url(record)
      "/#{context[:current_user].role.key}/messages/#{record.id}"
    end

    def body(record)
      record.body
    end

    def created_at(record)
      record.created_at.localize_stamp("20:20, January 10")
    end

    def remove_url(record)
      "/teacher/messages/#{record.id}"
    end
  end
end
