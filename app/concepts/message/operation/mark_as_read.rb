class Message::MarkAsRead < Trailblazer::Operation
	def process(options, params:, **)
		conversation = options["current_user"].mailbox.conversations.find_by( params[:id] )
		conversation.mask_as_read( options["current_user"])
	end
end

  # class MarkAsRead < Trailblazer::Operation
  #   # include Trailblazer::Operation::Policy
  #   # policy AnnouncementPolicy, :show?

  #   def process(params)
  #     conversation = params[:current_user].mailbox.conversations.find_by(id: params[:id])
  #     conversation.mark_as_read(params[:current_user]) 
  #   end
  # end