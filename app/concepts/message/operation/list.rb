class Message::List < Trailblazer::Operation
	step	:model!

	def model!(options, params:, **)
		options["model"] = options["current_user"].mailbox.send( params.fetch(:action, :inbox) )
	end
end