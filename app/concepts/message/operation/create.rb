class Message::Create < Trailblazer::Operation
  step  Nested( Message::Build )
  step  Contract::Build(constant: Message::Contract::Create)
  step  Contract::Validate() 
  step  :process!

  def process!(options, params:, **)
    options["model"].to = assign_recipients(options, params)
    options["current_user"].send_message(options["model"].to, params.fetch(:body), params.fetch(:subject))
  end
 def assign_recipients(options, params)

    recipients = []

    recipients << User.where( id: sanitize(params, :to) )
    unless student?(options)
      recipients << ( Room.where( id: sanitize(params, :rooms) ) || [] ).collect{|room| room.users}
      recipients << ( Room.where( id: sanitize(params, :parents) ) || [] ).collect{|room| room.users.collect(&:parents)}
    end
    recipients.flatten.uniq
  end

  def sanitize(params, key)
    params.fetch(key, []).reject(&:blank?)
  end

  def student?(options)
    options["model"].user.is_student?  #TODO: should it be coming from options hash?
  end
end
# class Create < Trailblazer::Operation

#     # include Trailblazer::Operation::Policy
#     # policy MessagePolicy, :create?

#     contract Contract::Create

#     def process(params)
#       reform = params.fetch(:reform)
#       model.to = assign_recipients(params)
#       validate(params[:reform]) do
#         params[:current_user].send_message(model.to, reform.fetch(:body), reform.fetch(:subject))
#       end
#     end

#     def setup_model!(params)
#       model.user = params.fetch(:current_user)
#     end

#     def model!(params)
#       OpenStruct.new(user:nil, to: [], rooms: nil, parents: nil, subject: nil, body: nil)
#     end

#     def assign_recipients(params)
#       reform = params.fetch(:reform)

#       recipients = []

#       recipients << User.where( id: sanitize(reform, :to) )
#       unless student?
#         recipients << ( Room.where( id: sanitize(reform, :rooms) ) || [] ).collect{|room| room.users}
#         recipients << ( Room.where( id: sanitize(reform, :parents) ) || [] ).collect{|room| room.users.collect(&:parents)}
#       end
#       recipients.flatten.uniq
#     end

#     def sanitize(params, key)
#       params.fetch(key, []).reject(&:blank?)
#     end

#     def student?
#       model.user.is_student?
#     end
#   end