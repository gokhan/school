class Message::Reply < Trailblazer::Operation
  step  :model!
  step  :setup_model!
  step  Contract::Build(constant: Message::Contract::Reply)
  step  Contract::Validate() 
  step  :process!

  def model!(options, params:, **)
    options["model"] = OpenStruct.new(user:nil, conversation: nil, body: nil)
  end
  
  def process!(options, params:, **)
    options["current_user"].reply_to_conversation( options["model"].conversation, params.fetch(:body) )
  end

  def setup_model!(options, params:, **)
    options["model"].user = options["current_user"]
    options["model"].conversation = Mailboxer::Conversation.find(params.fetch(:id))      
  end
end
  # class Reply < Create
  #   contract Contract::Reply

  #   def process(params)
  #     reform = params.fetch(:reform)
  #     validate(params[:reform]) do
  #       params[:current_user].reply_to_conversation( model.conversation, reform.fetch(:body) )
  #     end
  #   end

  #   def setup_model!(params)
  #     model.user = params.fetch(:current_user)
  #     model.conversation = Mailboxer::Conversation.find(params.fetch(:id))      
  #   end

  #   def model!(params)
  #     OpenStruct.new(user:nil, conversation: nil, body: nil)
  #   end  
  # end