class Message::Show < Trailblazer::Operation
	step	:model!

	def model!(options, params:, **)
		options["model"] = options["current_user"].mailbox.conversations.find( params[:id] )
	end
end

  # class Show < Trailblazer::Operation
  #   # include Trailblazer::Operation::Policy
  #   # policy AnnouncementPolicy, :show?

  #   def model!(params)
  #     params[:current_user].mailbox.conversations.find_by(id: params[:id])
  #   end
  # end