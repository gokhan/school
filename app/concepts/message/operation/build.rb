class Message::Build < Trailblazer::Operation
  step  :model!
  step  :assign_current_user!
  def model!(options, params:, **)
    options["model"] = OpenStruct.new(user:nil, to: [], rooms: nil, parents: nil, subject: nil, body: nil)
  end

  def assign_current_user!(options)
    options["model"].user =  options["current_user"]
  end
end