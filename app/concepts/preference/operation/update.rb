class Preference::Update < Trailblazer::Operation
  class Present < Trailblazer::Operation
    step  :model!
    step  Policy::Pundit( PreferencePolicy, :update? )
    step  Contract::Build( constant: Preference::Contract::Create )

    def model!(options, **)
    	options["model"] = options["current_user"].preference
    end
  end	
	step  	Nested( Present )
	step  	Contract::Validate(key: "preference")
	failure Contract::Persist(method: :sync)
	step  	Contract::Persist()
end