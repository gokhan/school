module Preference::Cell
  class List < Trailblazer::Cell
    #include Homework::Cell::Helpers    

    def course_url(record)
      "/teacher/courses/#{record.id}"
    end

    def course_edit_url(record)
      "/teacher/courses/#{record.id}/edit"
    end

    def avatar_url(record)
      record.avatar.data.square.url
    end    

    def room_name(course)
      course.room.name
    end

    def homework_count(record)
      record.homeworks.count
    end

    def video_count(record)
      record.videos.count
    end
  end
end
