require "reform/form/dry"

module Preference::Contract
  class Create < Reform::Form
    feature Reform::Form::Dry

    property :homework_notification_by_phone
    property :homework_notification_by_email
    property :announcement_notification_by_phone
    property :announcement_notification_by_email    
  end
end
