class AppSchema < Dry::Validation::Schema
  configure do |config|
    config.messages_file = '/my/app/config/locales/en.yml'
    config.messages = :i18n
  end

  def email?(value)
    true
  end

  define! do
    # define common rules, if any
  end
end