module School::Cell
  class Errors < Trailblazer::Cell
    include ::Cell::Slim

    def errors
    	model.errors.messages
    	# => options[:errors].messages || OpenStruct.new(messages: {})
    end
	end
end
