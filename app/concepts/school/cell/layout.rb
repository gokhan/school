module School::Cell
  class Layout < Trailblazer::Cell
    include ActionView::Helpers::CsrfHelper

    def course
      return context[:course] if context[:course]
    end

    def homework
      return context[:homework] if context[:homework]
    end

    def sidebar
      case context[:current_user].role.key.to_sym
        when :teacher 
          cell(Menu::Cell::Teacher)
        when :admin 
          cell(Menu::Cell::Admin)
        when :student 
          cell(Menu::Cell::Student)
        when :parent
          cell(Menu::Cell::Parent)
        else
          raise "Can't find a menu item for this role"
      end
    end
  end
end