module School::Cell
	module Helpers
		def self.included(base)
			base.send :include, ActionView::Helpers::DateHelper
			base.send :include, ActionView::Helpers::TextHelper
			base.send :include, ActionView::Helpers::FormOptionsHelper
			base.send :include, ActionView::Helpers::FormHelper
			base.send :include, ActionView::RecordIdentifier
		end

		def full_name(user)
			[user.first_name, user.last_name].compact.join(' ')
		end

    def errors
      options["errors"]
    end
	end
end
