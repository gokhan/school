require 'representable/json'

class ImageRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :data do
    property :thumb
  end
end 