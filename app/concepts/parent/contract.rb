module UserParent::Contract
  require "reform/form/dry"

  class Create < Reform::Form
    model UserParent

    feature Reform::Form::Dry

    property :user_id
    property :parent_id

    validation :default do
      required(:user_id).filled
      required(:parent_id).filled
    end
  end
end
