module Parent::Cell
  class List < Trailblazer::Cell
    include School::Cell::Helpers

    def user
      options[:user]
    end

    def collection
      @model.model
    end

    def remove_url(record)
      "/admin/users/#{user.id}/parents/#{record.id}/remove"
    end

    def avatar_url(record)
      record.avatar.data.square.url
    end    

    def role(record)
      record.role.name
    end
  end
end
