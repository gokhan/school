class UserParent::Create < Trailblazer::Operation
  extend Contract::DSL
  contract UserParent::Contract::Create

  step  Model( UserParent, :new )
  step  Contract::Build()
  step  Contract::Validate()
  step  Contract::Persist()
end

# class Create < Trailblazer::Operation
#   include Model
#   model UserParent, :create

#   contract Contract::Create

#   def process(params)
#     validate(params[:user]) do
#       contract.save
#     end
#   end

# end