class User::Update < Trailblazer::Operation
	class Present < Trailblazer::Operation
	  step  Model( User, :find_by)
	  step  Policy::Pundit( UserPolicy, :create? )
	  step  Contract::Build( constant: User::Contract::Create )
	end

	step  	Nested( Present )
	step  	Contract::Validate(key: "user")
	failure Contract::Persist(method: :sync)
  step  	Contract::Persist()
end