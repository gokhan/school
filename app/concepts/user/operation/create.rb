class User::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation
	  step  Model( User, :new )
	  step  Policy::Pundit( UserPolicy, :create? )
	  step  Contract::Build( constant: User::Contract::Create )
	end	
	
	step  Nested( Present )
  step  Contract::Validate(key: "user")
  failure Contract::Persist( method: :sync )
  step  Contract::Persist()
  step  :create_preference!

  def create_preference!(options, params:, **)
  	options["model"].create_preference
  end
end