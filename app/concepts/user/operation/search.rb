class User::Search < Trailblazer::Operation
  step  :model!
  #step  Policy::Pundit( UserPolicy, :search? )
  
  def model!(options, params:, **)
    options["model"] = type_filter(params)
                       .where("email ilike :q or users.name ilike :q" , { q: "%#{params[:q]}%" })
                       .order(:name).limit(20)
  end

  def type_filter(params)
    if type = params.fetch(:type, nil)
      User.includes(:role).where(roles: {key: type})
    else
      User.all    
    end
  end
end