module User::Parent
  class Add < Trailblazer::Operation
    step  Policy::Pundit( ParentPolicy, :add? )

    def process(params)
      if u = parent(params)
        ::UserParent.where(user_id: params.fetch(:user_id),
                         parent_id: u.id
                       ).first_or_create

      end
    end

    def parent(params)
      ::User.find_by(email: params.fetch(:email))
    end
  end
  
  class Remove < Trailblazer::Operation
    step  Policy::Pundit( ParentPolicy, :remove? )

    def process(params)
      ::UserParent.find_by(user_id: params.fetch(:user_id),
                       parent_id: params.fetch(:id),
                     ).destroy

    end
  end
end