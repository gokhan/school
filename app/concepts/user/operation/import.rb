module User::Tools end
class User::Tools::Import < Trailblazer::Operation
  step  Policy::Pundit( UserPolicy, :import? )

  COLUMNS = {name: 0, email: 1, role: 2, room: 3}

  def process(params)
      return unless sheet = open_spreadsheet(params[:file])

      count = 0
      (2..sheet.last_row).each do |i|
        row = sheet.row(i)
        result, op = User::Create.run(
                  user: { name: row[COLUMNS[:name]],
                          email: sanitize(row[COLUMNS[:email]]),
                          role_id: ( Role.find_by(name: row[COLUMNS[:role]]) || Role.find(2) ).id
                        }
                ) 
        if result
          assign_to_room(row, op.model)
          count += 1
        else
          model.error << parse_error(op)
        end
      end
      model.count = count
      model.result = true
  end

  def model!(options, params:, **)
    options["model"] = OpenStruct.new(result: false, count: 0, error: [])
  end

  private
  def open_spreadsheet(file)
    begin
      sheet = Roo::Spreadsheet.open(file)
    rescue ArgumentError
      model.result = false
      model.error << 'Sadece excel dosyasi kullanin ve dosyaniz asagida belirtilen formatta olsun.'
      return false
    end
  end

  def assign_to_room(row, user)
    if room = ::Room.find_by(name: row[COLUMNS[:room]])
      Room::User::Add.(room_id: room.id, user_id: user.id)
    end
  end

  def parse_error(op)
    op.errors.messages.collect{ |key, error| "#{op.contract.email}: #{key} #{error.join(', ')}"}.join(". ")
  end

  def sanitize(str)
    str.gsub(/<\/?[^>]*>/, "")
  end
end