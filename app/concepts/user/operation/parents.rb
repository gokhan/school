class User::Parents < Trailblazer::Operation
	step	:model!

	def model!(options, params:, **)
		options["model"] = User.find_by(params[:user_id]).parents
	end
end
  # class Parents < Trailblazer::Operation
  #   include Collection

  #   def model!(params)
  #     User.find( params.fetch(:user_id) ).parents
  #   end
  # end