class User::List < Trailblazer::Operation
	step  :model!
	step  Policy::Pundit( UserPolicy, :list? )

  def model!(options)
    options["model"] = User.includes(:role).order("roles.name desc")
  end
end