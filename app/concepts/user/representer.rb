require 'representable/json'

class UserRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :name
  property :email
  property :avatar do
    property :data do
      property :thumb
      property :square
    end
  end
end 