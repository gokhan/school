require "reform/form/dry"

module User::Contract
  class Create < Reform::Form
    feature Reform::Form::Dry
    EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

    property :name
    property :email
    property :role_id
    property :image, populate_if_empty: :populate_image! do
      property :data
    end

    def populate_image!(options) 
      model.image || Image.new
    end

    validation :default do
      configure do
        option :form
        config.messages_file = 'config/locales/error_messages.yml'
        def email?(value)          
          value =~ EMAIL_REGEX
        end

        def unique?(value)
          User.where.not(id: form.model.id).find_by(email: value).nil?
        end
      end

      #required(:email).filled(format?: EMAIL_REGEX)
      required(:name).filled
      required(:role_id).filled
      required(:email).filled(:email?, :unique?)
    end
  end

  class Update < Create
  end
end
