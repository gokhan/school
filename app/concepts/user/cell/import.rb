module User::Cell
  class Import < Trailblazer::Cell
    include School::Cell::Helpers

    def url
      options[:url]
    end

    private
    property :contract
  end
end
