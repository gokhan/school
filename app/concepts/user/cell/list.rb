module User::Cell
  class List < Trailblazer::Cell
    def edit_url(record)
      "/admin/users/#{record.id}/edit"
    end

    def parents_url(record)
      "/admin/users/#{record.id}/parents"
    end

    def avatar_url(record)
      record.avatar.data.square.url
    end

    def role(record)
      record.role.name rescue 'yok'
    end
  end
end
