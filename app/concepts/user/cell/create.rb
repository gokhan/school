module User::Cell
  class Create < Trailblazer::Cell
    include School::Cell::Helpers

    def url
      options[:url]
    end

    def errors
    	options["errors"]
    end
  end
end
