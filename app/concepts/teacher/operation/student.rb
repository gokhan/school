module Teacher::Student end;
class Teacher::Student::List < Trailblazer::Operation
  step  :model!
  step  Policy::Pundit( StudentPolicy, :list? )

  def model!(options)
    options["model"] = options["current_user"].courses.collect{|course| course.students}.flatten.uniq
  end
end
