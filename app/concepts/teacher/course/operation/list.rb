class Teacher::Course::List < Trailblazer::Operation
  step  :model!
  step  Policy::Pundit( CoursePolicy, :list? )

  def model!(options)
    options["model"] = options["current_user"].courses
  end
end
