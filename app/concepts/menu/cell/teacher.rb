module Menu::Cell
  class Teacher < Trailblazer::Cell
    include ActionView::Helpers::CaptureHelper

    def courses
      context[:current_user].courses
    end

    def students
      courses.map{|course| course.students}.flatten.uniq
    end

    def unread
      return [] #TODO
      context[:current_user].mailbox.inbox( {read: false} )
    end
  end
end
