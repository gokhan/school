module Menu::Cell
  class Parent < Trailblazer::Cell
    include ActionView::Helpers::CaptureHelper

    def children
      User.students.first(2)
    end

    def unread
      context[:current_user].mailbox.inbox( {read: false} )
    end
  end
end
