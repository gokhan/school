module Menu::Cell
  class Student < Trailblazer::Cell
    include ActionView::Helpers::CaptureHelper

    def all
      result = ::Student::Homework::All.({}, "current_user" => context[:current_user])
      result["model"]
    end

    def waiting
      result = ::Student::Homework::Waiting.({}, "current_user" => context[:current_user])
      result["model"]
      #::Student::Homeworks::Waiting.present(current_user: context[:current_user]).model
    end

    def expired
      result = ::Student::Homework::Expired.({}, "current_user" => context[:current_user])
      result["model"]
      #::Student::Homeworks::Expired.present(current_user: context[:current_user]).model
    end

    def unread
      return []
      context[:current_user].mailbox.inbox( {read: false} )
    end

    def room_url
      if context[:current_user].rooms.any?
        "/rooms/#{context[:current_user].rooms.first.id}"
      else
        "#"
      end
    end

    def schedule_url
      if context[:current_user].rooms.any?
        "/student/rooms/#{context[:current_user].rooms.first.id}/schedule"
      else
        "#"
      end
    end
  end
end
