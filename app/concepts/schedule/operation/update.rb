class Schedule::Update < Trailblazer::Operation
  # include Trailblazer::Operation::Policy
  # policy RoomPolicy, :list?
  step :model!
  step :process!
  def model!(options, params:, **)
    options["model"] = Schedule.find_or_initialize_by( room_id: params[:id], 
                                    day: params[:day],
                                    hour_id: params[:hour]
                                    )
  end

  def process!(options, params:, **)
    options["model"].course = Course.find(params[:course])
    options["model"].save
  end
end
