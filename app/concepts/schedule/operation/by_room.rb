class Schedule::ByRoom < Trailblazer::Operation
	step 	:model!

  def model!(options, params:, **)
    options["model"] = OpenStruct.new(room: Room.find(params.fetch(:id)) )
  end
end