module Schedule::Cell
  class Create < Trailblazer::Cell
    include School::Cell::Helpers

    def url
      options[:url]
    end

    def title
      'Sinif ' + ( @model.model.persisted? ? "guncelleme (#{@model.model.name})" : 'ekleme' )
    end
  end
end
