module Schedule::Cell
  class Users < Trailblazer::Cell
    def collection
      @model.model
    end

    def room
      @model.room
    end

    def title
      "#{room.name} sinifi ogrencileri"
    end

    def remove_url(record)
      "/admin/rooms/#{room.id}/users/#{record.id}/remove"
    end

    def avatar_url(record)
      record.avatar.data.square.url
    end  
    
    def role(record)
      record.role.name
    end
  end
end
