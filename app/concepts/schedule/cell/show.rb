module Schedule::Cell
  class Show < Trailblazer::Cell
    include School::Cell::Helpers

    def admin?
      context[:current_user].role.key == 'admin'
    end

    def days
      Schedule::DAYS
    end

    def hours
      Hour.all
    end

    def courses
      room.courses
    end

    def update_schedule_url
      "/admin/schedules/#{room.id}"
    end

    def schedule_for(day, hour)
      Schedule.find_by(room: room, day: day, hour: hour) 
    end

    def room
      context[:room]
    end

    def room_schedule_url(room)
      "/admin/schedules/#{room.id}"
    end

    def selected_class(r)
      room == r ? 'secondary' : ''
    end
  end
end
