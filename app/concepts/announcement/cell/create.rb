module Announcement::Cell
  class Create < Trailblazer::Cell
    include School::Cell::Helpers

    def announcement
    	model.model
    end

    def rooms
    	Room.all
    end

    def checked(room)
    	announcement.rooms.include?(room) ? "'checked'" : false
    end
  end
end
