module Announcement::Cell
  class List < Trailblazer::Cell

    def show
      if context[:current_user].role.key.to_sym == :student
        render 'list/list'
      else
        render 'list'
      end
    end

    def text(record)
      record.text
    end

    def created_at(record)
      record.created_at.localize_stamp("20:20, January 10")
    end

    def rooms(record)
      record.rooms.collect(&:name).join(", ")
    end

    def remove_url(record)
      "/teacher/messages/#{record.id}"
    end

  end
end
