module Announcement::Cell
  class Cardz < Trailblazer::Cell
    property :text
    property :user
    property :updated_at

    def announcement
      model
    end


    def random_avatar
      ['/avatar/jenny.jpg', '/avatar/joe.jpg', '/avatar/elliot.jpg', '/avatar/matt.jpg'].sample
    end    
  end
end
