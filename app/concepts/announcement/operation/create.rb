class Announcement::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation

	  step  Model( Announcement, :new )
	  step  Policy::Pundit( AnnouncementPolicy, :create? )
	  step  :assign_current_user!
	  step  Contract::Build( constant: Announcement::Contract::Create )
	  step  :prepopulate!
	  def prepopulate!(options)
	    options["contract.default"].prepopulate!
	  end

	  def assign_current_user!(options)
	    options["model"].user =  options["current_user"]
	  end
	end	

  step  Nested( Present )
  step  Contract::Validate(key: "announcement")
  failure  Contract::Persist(method: :sync)
  step  Contract::Persist()
end