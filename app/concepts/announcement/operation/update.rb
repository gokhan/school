class Announcement::Update < Trailblazer::Operation
	step  Nested( Announcement::Edit )
	step  Contract::Validate(key: "announcement")
  failure  Contract::Persist(method: :sync)
  step 	Contract::Persist()
end