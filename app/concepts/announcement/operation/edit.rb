class Announcement::Edit < Trailblazer::Operation
  extend Contract::DSL
  contract Announcement::Contract::Create

  step  Model( Announcement, :find_by )
  step  Policy::Pundit( AnnouncementPolicy, :update? )
  step  Contract::Build()
  step  :prepopulate!
  def prepopulate!(options)
    options["contract.default"].prepopulate!
  end
  
end