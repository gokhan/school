class Announcement::List < Trailblazer::Operation
  step  :model!
  step  Policy::Pundit( AnnouncementPolicy, :list? )

  def model!(options, params:, **)
    options["model"] = Announcement.all
  end
end