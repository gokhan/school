class Announcement::Show < Trailblazer::Operation
	step 	Model( Announcement, :find_by )
	step 	Policy::Pundit( AnnouncementPolicy, :show? )
end