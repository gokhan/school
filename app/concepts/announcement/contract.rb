require "reform/form/dry"

module Announcement::Contract
  class Create < Reform::Form
    feature Reform::Form::Dry

    property :text
    collection :room_ids

    validation :default do
      required(:text).filled
      required(:room_ids).filled
    end
  end
end