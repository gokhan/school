module Stat::Cell
  class Index < Trailblazer::Cell
    property :name
    property :description
    property :user
  end
end
