module Homework::Contract
  require "reform/form/dry"

  class Create < Reform::Form
    model Homework

    feature Reform::Form::Dry

    property :user
    property :course
    property :name
    property :description
    property :starts_at,  prepopulator: :starts_at!
    property :ends_at,  prepopulator: :ends_at!

    def starts_at!(options)
      return if model.starts_at.blank?
      self.starts_at = model.starts_at.strftime("%Y-%m-%d %H:%M") 
    end

    def ends_at!(options)
      return if model.ends_at.blank?
      self.ends_at= model.ends_at.strftime("%Y-%m-%d %H:%M") 
    end
    
    validation :default do
      required(:user).filled
      required(:course).filled
      required(:name).filled
      required(:starts_at).filled
      required(:ends_at).filled
    end
  end

  class Update < Create
  end
end
