module Homework::Cell
  class Create < Trailblazer::Cell
    include School::Cell::Helpers

    property :course

    def url
    	if model.model.new_record?
    		[:teacher, course, model.model]
    	else
    		[:teacher, model.model]
			end    		
    end
  end
end
