module Homework::Cell
  class Card < Trailblazer::Cell
    property :name
    property :description
    property :user
    property :ends_at

    def image
      ["http://static.pexels.com/wp-content/uploads/2014/07/alone-clouds-hills-1909-527x350.jpg"].sample
    end

    def edit_url
      "/teacher/homeworks/#{model.id}/edit"
    end
    
    def import_url
      "/teacher/homeworks/#{model.id}/tools/import_html"
    end

    def questions_url
      "/teacher/homeworks/#{model.id}/questions"
    end

    def starts_at
      return unless model.starts_at
      model.starts_at.localize_stamp("May 11th 13:12")
    end

    def ends_at
      return unless model.ends_at
      model.ends_at.localize_stamp("13:13 May 11")
    end

    def question_count
      model.questions.size
    end

    def class_name
      model.ends_at < DateTime.now ? 'past' : 'future'
    end
  end
end
