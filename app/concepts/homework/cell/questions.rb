module Homework::Cell
  class Questions < Trailblazer::Cell

    def homework
      context[:homework]
    end

    def collection
      @model.model
    end
  end
end
