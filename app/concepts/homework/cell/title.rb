module Homework::Cell
  class Title < Trailblazer::Cell
    property :name
    property :description
  end
end
