module Homework::Cell
  class Cardz < Trailblazer::Cell
    property :name
    property :description
    property :user
    property :ends_at
    property :updated_at

    def homework
      model
    end

    def random_avatar
      ['/avatar/jenny.jpg', '/avatar/joe.jpg', '/avatar/elliot.jpg', '/avatar/matt.jpg'].sample
    end    

    def take_url(homework)
      "/student/homeworks/%s/takes/new" % homework.id
    end
  end
end
