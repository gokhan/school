class Homework::Questions < Trailblazer::Operation
  step :model!
  step  Policy::Pundit( HomeworkPolicy, :questions? )

  def model!(options, params:, **)
    options["model"] = Homework.find(params[:homework_id]).questions.includes(:choices).order(:created_at)
  end
end
  # class Questions < Trailblazer::Operation
  #   include Collection
  #   include Trailblazer::Operation::Policy
  #   policy HomeworkPolicy, :questions?

  #   def model!(params)
  #     Homework.find(params[:homework_id]).questions.includes(:choices).order(:created_at)
  #   end
  # end
