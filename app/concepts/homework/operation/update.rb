class Homework::Update < Trailblazer::Operation
	class Present < Trailblazer::Operation
	  step  Model( Homework, :find_by )
	  step  Policy::Pundit( HomeworkPolicy, :create? )
	  step  Contract::Build( constant: Homework::Contract::Create )
	end	

  step  Nested( Present )
  step  Contract::Validate(key: "homework")
  failure  Contract::Persist(method: :sync)
  step  Contract::Persist()
end