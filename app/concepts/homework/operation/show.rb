class Homework::Show < Trailblazer::Operation
	step	:model!
	#step	Policy::Pundit( HomeworkPolicy, :show? )

	def model!(options, params:, **)
		options["model"] = Homework.find_by( id: params.fetch(:homework_id){ params.fetch(:id) } )
	end
end