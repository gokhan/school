# module Tools
#     class QuestionsImport < Trailblazer::Operation
#       include Trailblazer::Operation::Policy
#       policy HomeworkPolicy, :import?

#       def process(params)
#         begin
#           count = 0
#           Question.transaction do
#             sheet = Roo::Spreadsheet.open(params[:file])
#             header = sheet.row(1)
#             (0..sheet.last_row).each do |i|
#               row = sheet.row(i)
#               next if row[1].nil?
#               create_question(row, params)
#               count += 1
#             end
#             model.count = count
#             model.result = true
#           end
#         rescue ArgumentError
#           model.result = false
#           model.error = 'Sadece excel dosyasi kullanin ve asagida belirtilen formatta olsun.'
#         end
#       end

#       def model!(params)
#         OpenStruct.new(result: false, count: 0, error: '')
#       end

#       private
#       def create_question(row, params)
#         Question::Create.(
#             question: { text: row[0],
#                         choices:  choices(row),
#             },
#             homework: Homework.find(params[:homework_id]),
#             current_user: params.fetch(:current_user)
#           )
#       end

#       def choices(row)   
#         row.slice(1..5).each_with_index.map{|text, ind| {"text" => text, correct: (row[6] == ind+1) } }
#       end
#     end
#   end