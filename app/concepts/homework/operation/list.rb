class Homework::List < Trailblazer::Operation
  step :model!
  step Policy::Pundit( HomeworkPolicy, :list? )

  def model!(options, params:, **)
    homeworks = Homework.all
    options["model"] = case params[:status]
                        when 'waiting'
                          homeworks.where( "end_at > ?", DateTime.now )
                        when 'results'
                          homeworks.where( "end_at < ?", DateTime.now )
                        else
                          homeworks
                        end.order( ends_at: :desc)   
  end
end
  # class List < Trailblazer::Operation
  #   include Trailblazer::Operation::Policy
  #   policy HomeworkPolicy, :list?

  #   include Collection

  #   def model!(params)
  #     homeworks = Homework.all
  #     case params[:status]
  #       when 'waiting'
  #         homeworks = homeworks.where("ends_at >= ?", DateTime.now )
  #       when 'results'
  #         homeworks = homeworks.where("ends_at < ?", DateTime.now)
  #     end
  #     homeworks.order(ends_at: :desc)
  #   end
  # end