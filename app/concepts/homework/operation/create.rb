class Homework::Create < Trailblazer::Operation
	class Present < Trailblazer::Operation
	  step  Model( Homework, :new )
	  step  Policy::Pundit( HomeworkPolicy, :create? )
	  step  :assign!
	  step  Contract::Build( constant: Homework::Contract::Create )

	  def assign!(options, params:, **)
	    options["model"].user =  options["current_user"]
	    options["model"].course = Course.find(params[:course_id])
	  end
	  # def setup_params!(params)
	  #   params[:starts_at] = Chronic::parse(params[:starts_at]) if params[:starts_at]
	  #   params[:ends_at] = Chronic::parse(params[:ends_at]) if params[:ends_at]
	  # end
	end	

  step  Nested( Present )
  step  Contract::Validate(key: "homework")
  failure  Contract::Persist(method: :sync)
  step  Contract::Persist()
  step  :notify!

  def notify!(options, params:, **)
  	notifier = Notification::Create.("sourceable" => options["model"], "action" => "create")
  	NotifyWorker.perform_async(notifier["model"].id) 
  end
end