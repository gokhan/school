class Question::Show < Trailblazer::Operation
	step  Model( Question, :find_by)
	step  Policy::Pundit( QuestionPolicy, :show? )
end

  # class Show < Trailblazer::Operation
  #   include Model
  #   model Question, :find

  #   include Trailblazer::Operation::Policy
  #   policy OptionPolicy, :show?

  # end