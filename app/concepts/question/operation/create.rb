class Question::Create < Trailblazer::Operation
  extend Contract::DSL
  contract Question::Contract::Create

	step 	Model( Question, :new )
	step  Policy::Pundit( QuestionPolicy, :create? )
  step  :assign_current_user!
  step  Contract::Build()
  step  Contract::Validate()
  step  Contract::Persist()
  step  :add_to_homework!

  def assign_current_user!(options)
  	options["model"].user =  options["current_user"]
  end

  def add_to_homework!(options)
    options["model"].homeworks << operation.contract.homework
  end
end

  # class Create < Trailblazer::Operation
  #   include Model
  #   model Question, :create

  #   include Trailblazer::Operation::Policy
  #   policy QuestionPolicy, :create?
    
  #   include Dispatch
  #   callback :default, Callback::Default

  #   contract Contract::Create

  #   def process(params)
  #     validate(params[:question]) do
  #       contract.save
  #       dispatch!
  #     end
  #   end

  #   def setup_model!(params)
  #     model.user = params.fetch(:current_user)
  #     contract.homework = params.fetch(:homework, nil)
  #   end
  # end

#   module Question::Callback
#   class Default < Disposable::Callback::Group
#     on_create :add_to_homework!
#     collection :choices do #TODO: how to assign correct option
#       on_change :update_correct_choice!
#     end

#     def add_to_homework!(question, operation:, **)
#       operation.model.homeworks << operation.contract.homework
#     end

#     def update_correct_choice!(choice, operation:, **)
#       if choice.correct
#         operation.model.update_attribute(:correct_id, choice.model.id)
#       end
#     end
#   end
# end