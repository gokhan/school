class Question::List < Trailblazer::Operation
	step  :model!
	step  Policy::Pundit( QuestionPolicy, :list? )
  
	def model!(options, params:, **)
		options["model"] = Homework.find_by(params[:question_id]).questions
	end
end

  # class List < Trailblazer::Operation
  #   include Collection

  #   include Trailblazer::Operation::Policy
  #   policy OptionPolicy, :list?

  #   def model!(params)
  #     Homework.find(params[:question_id]).questions
  #   end
  # end