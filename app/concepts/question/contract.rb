module Question::Contract
  require "reform/form/dry"

  class Create < Reform::Form
    model Question

    feature Reform::Form::Dry
    feature Disposable::Twin::Persisted
    
    property :user
    property :text
    property :homework, virtual: true
    property :tag_list
    
    property :image, populate_if_empty: :populate_image! do
      property :data
    end

    def populate_image!(options) 
      model.image || Image.new
    end

    collection :choices, 
          prepopulator: :prepopulate_choices!, 
          populate_if_empty: :populate_choices!,
          skip_if:           :all_blank do 
      property :text
      property :correct, virtual: true
      property :id, skip_if: lambda { |fragment, *| fragment["id"].blank? }
    end

    def prepopulate_choices!(options)
      choices.each do |choice|
        if choice.model.question.correct_id == choice.id
          choice.correct = 1
        end
      end

      (5 - choices.size).times { choices << Option.new }
    end

    def populate_choices!(fragment)
      Option.find_by(id: fragment[:id]) or Option.new
    end

    validation :default do
      required(:user).filled
      required(:text).filled
    end
  end

  class Update < Create
  end
end
