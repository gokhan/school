module Question::Cell
  class List < Trailblazer::Cell

    def collection
      @model.model
    end
  end
end
