module Question::Cell
  class Card < Trailblazer::Cell
    property :id
    property :text
    property :user
    property :choices
    property :homeworks
    property :correct

    def url
      '#'
    end
    
    def avatar_url
      model.avatar.data.square.url
    end

    def choice_avatar_url(choice)
      choice.avatar.data.square.url
    end

    def class_choice(choice)
      if correct == choice
        'correct'
      end
    end

    def edit_url
      "/teacher/questions/#{id}/edit"
    end
  end
end
