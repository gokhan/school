module Question::Cell
  class Show < Trailblazer::Cell
    property :text
    property :user
  end
end
