require 'representable/json'

class QuestionRepresenter < Representable::Decorator
  include Representable::JSON

  property :id
  property :text
  property :avatar do
    property :data do
      property :thumb
      property :square
    end
  end
end 