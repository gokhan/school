class Option::Update < Option::Create
	step	Model( Option, :find_by), override: true
	step  Policy::Pundit( AnnouncementPolicy, :update? ), override: true
end