class Option::Create < Trailblazer::Operation
  require_relative '../contract'
  extend Contract::DSL
  contract ::Option::Contract::Create

  step  Model( Option, :new )
  step  Policy::Pundit( OptionPolicy, :create? )
  step  Contract::Build()
  step  Contract::Validate()
  step  Contract::Persist()
end

  # class Create < Trailblazer::Operation
  #   include Model
  #   model Option, :create

  #   include Trailblazer::Operation::Policy
  #   policy OptionPolicy, :create?


  #   contract Contract::Create

  #   def process(params)
  #     validate(params[:option]) do
  #       contract.save
  #     end
  #   end
  # end