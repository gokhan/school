module Option::Contract
  require "reform/form/dry"

  class Create < Reform::Form
    model Option

    feature Reform::Form::Dry
    feature Disposable::Twin::Persisted
    
    property :question
    property :text
   
    
    property :image, populate_if_empty: :populate_image! do
      property :data
    end

    def populate_image!(options) 
      model.image || Image.new
    end 
  end

  class Update < Create
  end
end
