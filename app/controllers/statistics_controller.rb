class StatisticsController < ApplicationController
  def index
    render html: cell(Stat::Cell::Index, nil, 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

end
