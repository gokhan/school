module MessageActions
  extend ActiveSupport::Concern

  def index
    result = run Message::List  #(params.merge(box: :conversations))
    render html: cell(Message::Cell::List, result["model"], 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def inbox    
    render_list
  end

  def sent
    render_list
  end

  def trash
    render_list
  end

  def show
    reply  = run Message::Reply
    message = run Message::Show
    Message::MarkAsRead.call
    render html: cell(Message::Cell::Show, message["model"], 
                      operation: reply["model"],
                      url: paths(:reply, message["model"].id),
                      context: default_context(),
                      layout: School::Cell::Layout)

  end

  # New actions
  def new
    result = run Message::Build
    render html: cell(Message::Cell::Create, result["model"], 
                      url: paths(:index), 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def create
    run Message::Create do |op|
      return redirect_to(paths(:index))
    end
    render html: cell(Message::Cell::Create, @operation, 
                      url: paths(:index), 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def reply
    run Message::Reply do |result|
      return redirect_to(paths(:show, result["model"].conversation))
    end
  end

  protected
  def render_list
    result = run Message::List
    render html: cell(Message::Cell::List, result["model"], 
                  context: default_context(),
                  layout: School::Cell::Layout)    

  end

  def paths(sym, id=nil)
    {
      index: "/#{who}/messages",
      reply: "/#{who}/messages/#{id}/reply",
      show:  "/#{who}/messages/#{id}",
    }.fetch(sym)
  end

end