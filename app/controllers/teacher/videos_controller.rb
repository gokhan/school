class Teacher::VideosController < ApplicationController
  def index
    collection = Video::List.present(params.merge!(id: course.id))
    render html: cell(Video::Cell::List, collection, 
                      context: default_context( { course: course } ), 
                      layout: School::Cell::Layout)
  end

  def new
    form Video::Create
    render html: cell(Video::Cell::Create, @operation, 
                      url: teacher_course_videos_path(course), 
                      context: default_context( { course: course } ),
                      layout: School::Cell::Layout)
  end

  def create
    run Video::Create do |op|
      return redirect_to(teacher_course_path(@model.videoable))
    end
    render html: cell(Video::Cell::Create, @operation, 
                      url: teacher_course_videos_path(course), 
                      context: default_context( { course: course } ),
                      layout: School::Cell::Layout)
  end

  def edit
    form Video::Update
    render html: cell(Video::Cell::Create, @operation, 
                      url: teacher_video_path(@model), 
                      context: default_context( { course: @model.videoable } ),
                      layout: School::Cell::Layout)
  end

  def update
    run Video::Update do |op|
      return redirect_to(teacher_course_path(@model.videoable))
    end
    render html: cell(Video::Cell::Create, @operation, 
                      url: teacher_course_video_path(course, @model), 
                      context: default_context( { course: @model.videoable } ),
                      layout: School::Cell::Layout)
  end

  private
  def course
    Course::Show.present(params.merge(id: params.fetch(:course_id))).model
  end
end
