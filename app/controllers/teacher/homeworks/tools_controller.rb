class Teacher::Homeworks::ToolsController < ApplicationController
  def import_html
    render html: cell(Homework::Cell::QuestionsImport, 
                      @operation, 
                      context: default_context( { homework: homework } ),
                      url: import_teacher_homework_tools_path(homework),
                      layout:  School::Cell::Layout)
  end

  def import
    render json: Homework::Tools::QuestionsImport.(params).model
  end


  private
  def homework
    Homework::Show.present(params.merge(id: params.fetch(:homework_id))).model
  end
  
end
