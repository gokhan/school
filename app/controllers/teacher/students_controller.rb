class Teacher::StudentsController < ApplicationController
  def index
    result = run Teacher::Student::List
    render html: cell(User::Cell::List, result["model"], 
                      context: default_context(),
                      layout: School::Cell::Layout)

  end
end