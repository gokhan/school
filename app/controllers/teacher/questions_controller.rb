class Teacher::QuestionsController < ApplicationController
  before_filter :add_homework_to_params, only: [:new, :create]

  def index
    collection = Homework::Questions.present(params)
    render html: cell(Homework::Cell::Questions, collection, 
                      context: default_context( {homework: homework, course: homework.course } ),
                      layout: School::Cell::Layout)  
  end

  def new
    form Question::Create
    render html: cell(Question::Cell::Create, @operation, url: teacher_homework_questions_path(homework),
                      context: default_context( {homework: homework, course: homework.course } ),
                      layout: School::Cell::Layout)  

  end

  def create
    run Question::Create do |op|
      return redirect_to(teacher_homework_questions_path(homework))
    end
    render html: cell(Question::Cell::Create, @operation, url: teacher_questions_path(homework),
                      context: default_context( {homework: homework } ),
                      layout: School::Cell::Layout)  

  end

  def edit
    form Question::Update
    render html: cell(Question::Cell::Create, @operation, 
                      url: teacher_question_path(@model), 
                      context: default_context(),
                      layout: School::Cell::Layout)  

  end

  def update
    run Question::Update do |op|
      return redirect_to(teacher_homework_questions_path(@model.homeworks.first))
    end
    render html: cell(Question::Cell::Create, @operation, url: teacher_question_path(homework, @model),
                      context: default_context( {homework: homework, course: homework.course } ),
                      layout: School::Cell::Layout)  
  end


  # avatar upload
  def avatar
    run Question::Update do |op|
      return( render json: QuestionRepresenter.new(op.model) )
    end
    return( render json: @operation.contract.errors )
  end

  private
  def homework
    Homework::Show.present(params.merge(id: params.fetch(:homework_id))).model
  end

  def add_homework_to_params
    params.merge!(homework: homework)
  end
  
  def deault_context
    {role: :teacher}
  end
end
