class Teacher::AnnouncementsController < ApplicationController
  def index
    result = run Announcement::List
    render html: cell(Announcement::Cell::List, result["model"], 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end
  
  # New actions
  def new
    result = run Announcement::Create::Present
    # raise result.inspect
    render html: cell(Announcement::Cell::Create, @form,
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def create
    run Announcement::Create do |op|
      return redirect_to(teacher_announcements_path)
    end
    render html: cell(Announcement::Cell::Create, @form,
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  # New actions
  def edit
    result = run Announcement::Edit
    render html: cell(Announcement::Cell::Create, @form,
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def update
    run Announcement::Update do |op|
      return redirect_to(teacher_announcements_path)
    end
    render html: cell(Announcement::Cell::Create, @form,
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def destroy
    run Announcement::Destroy do |op|
      return redirect_to(teacher_announcements_path)
    end
    return redirect_to(teacher_announcements_path)
  end
end
