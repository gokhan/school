class Teacher::OptionsController < ApplicationController
  # avatar upload
  def avatar
    run Option::Update do |op|
      return( render json: OptionRepresenter.new(op.model) )
    end
    return( render json: @operation.contract.errors )
  end

end
