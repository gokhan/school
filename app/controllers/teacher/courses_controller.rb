class Teacher::CoursesController < ApplicationController
  def index
    result = run Teacher::Course::List
    render html: cell(Course::Cell::List, result["model"], 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def show
    result = run Course::Show
    render html: cell(Course::Cell::Show, result["model"], 
                      context: default_context({ course: result["model"] }),
                      layout: School::Cell::Layout)
  end
  
  # New actions
  def new
    result = run Course::Create::Present
    render html: cell(Course::Cell::Create, @form,
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def create
    run Course::Create do |op|
      return redirect_to(teacher_courses_path)
    end
    render html: cell(Course::Cell::Create, @form, 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  # Update actions
  def edit
    result = run Course::Update::Present
    render html: cell(Course::Cell::Create, @form, 
                      context: default_context( { course: result["model"] } ),
                      layout: School::Cell::Layout)
  end

  def update
    run Course::Update do |op|
      return redirect_to(teacher_courses_path)
    end
    render html: cell(Course::Cell::Create, @form, 
                      context: default_context( { course: result["model"] } ),
                      layout: School::Cell::Layout)
  end


  # avatar upload
  def avatar
    run Course::Update do |op|
      return( render json: CourseRepresenter.new(op.model) )
    end
    return( render json: @operation.contract.errors )
  end

end
