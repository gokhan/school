class Teacher::HomeworksController < ApplicationController
  def index
    result = run Course::Homeworks
    render html: cell(Homework::Cell::List, result["model"], 
                      context: default_context( { course: course } ), 
                      layout: School::Cell::Layout)
  end

  def new
    result = run Homework::Create::Present
    render html: cell(Homework::Cell::Create, @form, 
                      context: default_context( { course: course } ),
                      layout: School::Cell::Layout)
  end

  def create
    run Homework::Create do |result|
      return redirect_to(teacher_course_path(course))
    end
    render html: cell(Homework::Cell::Create, @form, 
                      context: default_context( { course: course } ),
                      layout: School::Cell::Layout)
  end

  def edit
    result = run Homework::Update::Present
    render html: cell(Homework::Cell::Create, @form, 
                      context: default_context( { course: result["model"].course } ),
                      layout: School::Cell::Layout)
  end

  def update
    run Homework::Update do |result|
      return redirect_to(teacher_course_path(result["model"].course))
    end
    render html: cell(Homework::Cell::Create, @form, 
                      context: default_context( { course: course } ),
                      layout: School::Cell::Layout)
  end

  private
  def course
    Course::Show.(id: params.fetch(:course_id))["model"]
  end
end
