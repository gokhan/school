class Admin::Users::ToolsController < ApplicationController
  def import_html
    render html: cell(User::Cell::Import, 
                      @operation, 
                      url: import_users_path, 
                      context: default_context(),
                      layout:  School::Cell::Layout)
  end

  def import
    render json: User::Tools::Import.(params).model
  end
  
end
