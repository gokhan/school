class Admin::RoomsController < ApplicationController
  def index
    result = run Room::List
    render html: cell(Room::Cell::List, result["model"], 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def new
    result = run Room::Create::Present
    render html: cell(Room::Cell::Create, @form, 
                      context: default_context(),
                      layout:  School::Cell::Layout)
  end

  def create
    run Room::Create do |op|
      return redirect_to(admin_rooms_path)
    end
    render html: cell(Room::Cell::Create, @form, 
                      "errors"=>result["contract.default"].errors,                                           
                      context: default_context(), 
                      layout:  School::Cell::Layout)
   end

  def edit
    result = run Room::Update::Present
    render html: cell(Room::Cell::Create, @form, 
                      context: default_context(), 
                      layout:  School::Cell::Layout)
  end

  def update
    run Room::Update do |op|
      return redirect_to(admin_rooms_path)
    end
    render html: cell(Room::Cell::Create, @form, 
                      "errors"=>result["contract.default"].errors,                      
                      context: default_context(), 
                      layout:  School::Cell::Layout)
  end 
end
