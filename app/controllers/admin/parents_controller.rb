class Admin::ParentsController < ApplicationController
  def index
    user = User::Show.present(params.merge(id: params.fetch(:user_id))).model
    collection = User::Parents.present(params)
    render html: cell(Parent::Cell::List, 
                      collection, 
                      user: user, 
                      context: default_context(),
                      layout:  School::Cell::Layout)
  end

  def create
    model = UserParent::Create.(params).model
    render json: model
  end


  def add
   User::Parent::Add.(params) 
   render json: {success: true}
  end

  def remove
   User::Parent::Remove.(params) 
   redirect_to action: :index
  end
end
