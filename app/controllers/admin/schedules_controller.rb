class Admin::SchedulesController < ApplicationController
  def show
    result = run Schedule::ByRoom
    render html: cell(Schedule::Cell::Show, result["model"], 
                      context: default_context(room: result["model"].room),
                      layout: School::Cell::Layout)
  end  

  def update
    run Schedule::Update do |op|
      return redirect_to :back
    end
  end

end
