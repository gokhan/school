class Admin::UsersController < ApplicationController
  def index
    result = run User::List
    render html: cell(User::Cell::List, result["model"], context: default_context(), layout: School::Cell::Layout)
  end

  def new
    result = run User::Create::Present
    render html: cell(User::Cell::Create, @form, 
                      context: default_context(),
                      layout:  School::Cell::Layout)
  end

  def create
    run User::Create do |result|
      return redirect_to(admin_users_path)
    end
    render html: cell(User::Cell::Create, @form, 
                      context: default_context(), 
                      layout:  School::Cell::Layout)
  end

  def edit
    result = run User::Update::Present
    render html: concept(User::Cell::Create, @form, 
                      context: default_context(), 
                      layout:  School::Cell::Layout)
  end

  def update
    run User::Update do |result|
      return redirect_to(admin_users_path)
    end

    render html: cell(User::Cell::Create, @form, 
                      context: default_context(), 
                      layout:  School::Cell::Layout)
  end 

  # avatar upload
  def avatar
    run User::Update do |result|
      return( render json: UserRepresenter.new(result["model"]) )
    end
    return( render json: result )
  end

  # user search
  def search
    result = run User::Search
    render( json: UserRepresenter.for_collection.new(result["model"]) )
  end
end

