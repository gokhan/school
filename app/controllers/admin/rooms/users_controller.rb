class Admin::Rooms::UsersController < ApplicationController
  def index
    result = run Room::User::List
    render html: cell(Room::Cell::Users, result["model"],
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def add
    Room::User::Add.(params) 
    render json: {success: true}
  end

  def remove
    Room::User::Remove.(params) 
    redirect_to action: :index
  end

end
