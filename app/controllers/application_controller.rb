class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_user
    if request.env['PATH_INFO'][/\A\/admin/]
      Role.find_by(key: :admin).users.first
    elsif request.env['PATH_INFO'][/\A\/teacher/]
      Role.find_by(key: :teacher).users.first
    elsif request.env['PATH_INFO'][/\A\/parent/]
      Role.find_by(key: :parent).users.first      
    else
      Role.find_by(key: :student).users.first
    end
  end

  private
  def _run_options(options)
    options.merge( "current_user" => current_user )
  end

  def default_context(add = {})
    { current_user: current_user }.merge(add)
  end

end
