class Student::AnnouncementsController < ApplicationController
  def index
    collection = Student::Announcement::List.present(params)
    render html: cell(Announcement::Cell::List, collection, 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end
end
