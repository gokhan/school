class Student::AnswersController < ApplicationController

  def create
    run Answer::FindOrCreate do |op|
      return( render json: {result: true, model: AnswerRepresenter.new(op.model) } )
    end

    return( render json: { result: false, errors: @operation.contract.errors } )    
  end

  private
end