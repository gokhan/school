class Student::HomeworksController < ApplicationController
  def index
    result = run Student::Homework::All
    render html: cell(Homework::Cell::List, result["model"], 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def waiting
    result = run Student::Homework::Waiting
    render html: cell(Homework::Cell::List, result["model"], 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def expired
    result = run Student::Homework::Expired
    render html: cell(Homework::Cell::List, result["model"], 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

end
