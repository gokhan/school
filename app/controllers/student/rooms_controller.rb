class Student::RoomsController < ApplicationController
  def schedule
    result = run Schedule::ByRoom
    render html: cell(Schedule::Cell::Show, result["model"], 
                      context: default_context(room: result["model"].room),
                      layout: School::Cell::Layout)
  end  
end
