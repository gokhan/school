class Student::TakesController < ApplicationController
  def index
    render html: cell(Homework::Cell::Take, homework, 
                      layout: School::Cell::Layout)
  end

  def new
    # params.merge!( take: {homework_id: homework.id} )
    run Take::FindOrCreate do |result|
      return redirect_to( student_homework_take_path(result["model"].homework, result["model"]) )
    end
  end


  def show
    result = run Take::Questions
    render html: cell(Take::Cell::Questions, result["model"], 
                      context: default_context( {homework: homework, course: homework.course, take: take } ),
                      layout: School::Cell::Layout)  
  end
  private
  def homework
    result = run Homework::Show
    result["model"]
  end

  def take
    result = run Take::Show
    result["model"]
  end
end
