class PreferencesController < ApplicationController
  # Update actions
  def edit
    result = run Preference::Update::Present
    render html: cell(Preference::Cell::Create, @form, 
                      context: default_context(),
                      layout: School::Cell::Layout)
  end

  def update
    run Preference::Update do |op|
      return redirect_to(edit_preference_path)
    end
    render html: cell(Preference::Cell::Create, @form, 
                      context: default_context( ),
                      layout: School::Cell::Layout)
  end  
end
