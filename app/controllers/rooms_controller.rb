class RoomsController < ApplicationController
  def show
    result = run Room::Show
    render html: cell(Room::Cell::Show, result["model"], 
                      context: default_context({ room: result["model"] }),
                      layout: School::Cell::Layout)
  end

end
