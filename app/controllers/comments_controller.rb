class CommentsController < ApplicationController
  
  def create
    run Comment::Create do |op|
      return redirect_to(room_path(op.model.commentable))
    end
  end
end
