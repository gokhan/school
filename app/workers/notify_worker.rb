class NotifyWorker
  include Sidekiq::Worker
  
  attr_accessor :notification
  
  def perform(notification_id)
  	@notification = Notification.find(notification_id)
  	case notification.sourceable
  	when Homework
  		notify_homework()
  	end
  end

  private
  def notify_homework
  	users_to_notify = notification.users.select{|user| user.preference.homework_notification_by_email? }
  	Notification::Send.(users: users_to_notify, notification: notification)
  end
end
