class VideoPolicy < ApplicationPolicy
  def create?
    admin? or teacher?
  end

  def update?
    create?
  end

  def show?
    create?
  end
  
  def list?
    create?
  end
end