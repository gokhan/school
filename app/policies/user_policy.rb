class UserPolicy < ApplicationPolicy
  def create?
    admin?
  end

  def update?
    create?
  end

  def list?
    admin? or teacher?
  end

  def search?
    list?
  end
  
  def import?
    admin?
  end  
end