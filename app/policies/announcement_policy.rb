class AnnouncementPolicy < ApplicationPolicy
  def create?
    admin? or teacher?
  end

  def update?
    create?
  end
  
  def show?
    create?
  end
  
  def list?
    true #admin? or teacher? 
  end
end