class ParentPolicy < ApplicationPolicy
  def add?
    admin?
  end

  def remove?
    admin?
  end
end