class OptionPolicy < ApplicationPolicy
  def create?
    admin? or teacher?
  end

  def update?
    create?
  end
end