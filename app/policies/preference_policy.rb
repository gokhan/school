class PreferencePolicy < ApplicationPolicy
  def update?
    admin? or record.user == user
  end
end