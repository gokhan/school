class CoursePolicy < ApplicationPolicy
  def create?
    admin? or teacher?
  end

  def update?
    create?
  end

  def show?
    create?
  end

  def homeworks?
    teacher?
  end
  
  def list?
    admin? or teacher?
  end
end