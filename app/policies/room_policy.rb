class RoomPolicy < ApplicationPolicy
  def create?
    admin?
  end

  def update?
    create?
  end

  def add?
    admin?
  end

  def remove?
    add?
  end
  
  def list?
    admin? or teacher?
  end

  def show?
    true #TODO: who can see a room
  end
end