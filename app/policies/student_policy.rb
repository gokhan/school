class StudentPolicy < ApplicationPolicy
  def homeworks?
    true
  end

  def list?
    admin? or teacher?
  end
end