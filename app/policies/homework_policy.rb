class HomeworkPolicy < ApplicationPolicy
  def create?
    admin? or teacher?
  end

  def update?
    create?
  end

  def show?
    create?
  end

  def questions?
    admin? or teacher?
  end

  def import
    teacher?
  end
  
  def list?
    admin? or teacher?
  end
end