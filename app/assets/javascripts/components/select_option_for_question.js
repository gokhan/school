$(function() {
  var $checkbox = $(".ui.radio.checkbox");

  $(".ui.radio.checkbox").click(function(){
    var take_id = $(this)
                        .closest('.take')
                        .data('id');
    var question_id = $(this)
                        .closest('.question')
                        .data('id');
    var option_id = $(this)
                      .data('id');

    $.ajax({
      type:'POST', 
      url: '/student/answers',
      headers: {'X-CSRF-Token': $("meta[name='csrf-token']").attr('content')},
      data: { answer: {
                        take_id: take_id,
                        question_id: question_id, 
                        option_id: option_id 
                      }
             },
      success:function(data) {
        $('.success').text(data.model.take.right);
        $('.alert').text(data.model.take.wrong);
        $('.warning').text(data.model.take.blank);
      }              
    });
  });

  $("input.option").click(function(){
    var take_id = $(this)
                        .closest('.take')
                        .data('id');
    var question_id = $(this)
                        .closest('.question')
                        .data('id');
    var option_id = $(this)
                      .data('id');

    $.ajax({
      type:'POST', 
      url: '/student/answers',
      headers: {'X-CSRF-Token': $("meta[name='csrf-token']").attr('content')},
      data: { answer: {
                        take_id: take_id,
                        question_id: question_id, 
                        option_id: option_id 
                      }
             },
      success:function(data) {
        $('.success').text(data.model.take.right);
        $('.alert').text(data.model.take.wrong);
        $('.warning').text(data.model.take.blank);
      }              
    });
  });
});