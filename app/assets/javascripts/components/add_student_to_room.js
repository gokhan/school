$(function() {
  var options = {
    url: function(q) {
      var type= $('form').find("input[name=type]").val();      
      return "/admin/users/search?q=" + q + "&type=" + type;
    },

    getValue: "email",

    template: {
      type: "custom",
      method: function(value, item) {
         return "<img class='ui middle aligned mini image' src='" + item.avatar.data.square.url + "' />  " + item.name+ ", " + value;
      }
    }

  };
  $("#add-student-to-room").easyAutocomplete(options);

  $('#add-student-to-room-form').bind('ajax:success', function(event, data, status, xhr){
    if(data.success){
      location.reload(false)
    }
  });


});
