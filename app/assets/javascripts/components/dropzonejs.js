$(function() {
  Dropzone.options.dropzoneForm = {
    init: function() {
            this.on("success", function(file, response) { 
                if (response.result){
                  alert(response.count + " soru sinava eklendi.");
                }
                else{
                  alert(response.error);
                }
            }); 
          }
  }

  Dropzone.options.dropzoneUserForm = {
    init: function() {
            this.on("success", function(file, response) { 
                if (response.result){
                  alert(response.count + " kullanici sisteme eklendi.");
                  if(response.error != ''){
                    alert(response.error.join("\n"));
                  }
                }
                else{
                  alert(response.error);
                }
            }); 
          }
  }


  $("img.image-user-dropzone").each(function(){
    new Dropzone(this, {
      url: '/admin/users/' + $(this).data('id') + '/avatar',
      paramName: 'user[image[data]]',
      headers: {'X-CSRF-Token': $("meta[name='csrf-token']").attr('content')},
      success: function(a, response){
        $("img[data-id='" + response.id +"']").attr('src', response.avatar.data.square.url);
        $("img[data-id='" + response.id +"']").transition('jiggle');
      }
    })
  })

  $("img.image-course-dropzone").each(function(){
    new Dropzone(this, {
      url: '/teacher/courses/' + $(this).data('id') + '/avatar',
      paramName: 'course[image[data]]',
      headers: {'X-CSRF-Token': $("meta[name='csrf-token']").attr('content')},
      success: function(a, response){
        $("img[data-id='" + response.id +"']").attr('src', response.avatar.data.square.url);
        $("img[data-id='" + response.id +"']").transition('jiggle');
      }
    })
  })

  $("img.image-question-dropzone").each(function(){
    new Dropzone(this, {
      url: '/teacher/questions/' + $(this).data('id') + '/avatar',
      paramName: 'question[image[data]]',
      headers: {'X-CSRF-Token': $("meta[name='csrf-token']").attr('content')},
      success: function(a, response){
        $("img[data-id='" + response.id +"']").attr('src', response.avatar.data.square.url);
        $("img[data-id='" + response.id +"']").transition('jiggle');
      }
    })
  })

  $("img.image-choice-dropzone").each(function(){
    new Dropzone(this, {
      url: '/teacher/options/' + $(this).data('id') + '/avatar',
      paramName: 'option[image[data]]',
      headers: {'X-CSRF-Token': $("meta[name='csrf-token']").attr('content')},
      success: function(a, response){
        $("img[data-id='" + response.id +"']").attr('src', response.avatar.data.square.url);
        $("img[data-id='" + response.id +"']").transition('jiggle');
      }
    })
  })

});