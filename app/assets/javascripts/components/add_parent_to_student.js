$(function() {
  var options = {
    url: function(q) {
      return "/admin/users/search?q=" + q + "&type=parent";
    },

    getValue: "email",

    template: {
      type: "custom",
      method: function(value, item) {
        return "<img class='ui middle aligned mini image' src='" + item.avatar.data.square.url + "' />  " + item.name+ ", " + value;
      }
    }

  };
  $("#add-parent-to-student").easyAutocomplete(options);

  $('#add-parent-to-student-form').bind('ajax:success', function(event, data, status, xhr){
    if(data.success){
      location.reload(false)
    }
  });
});
