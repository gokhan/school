$(function() {
  if (!$('#stack').length)
    return
  // Load the Visualization API and the corechart package.
  google.charts.load('current', {'packages':['corechart']});

  // Set a callback to run when the Google Visualization API is loaded.
  google.charts.setOnLoadCallback(drawChart);

  // Callback that creates and populates a data table,
  // instantiates the pie chart, passes in the data and
  // draws it.
  function drawChart() {

    // Create the data table.
    var data = google.visualization.arrayToDataTable([
      ['Sorular', 'Dogru', 'Yanlis', 'Bos', { role: 'annotation' } ],
      ['1. soru', 20, 10, 10, ''],
      ['2. soru', 18, 15, 7, ''],
      ['3. soru', 15, 20, 5, '']
    ]);

    var options = {
      width: 900,
      height: 200,
      is3D: true,
      legend: { position: 'right', maxLines: 3 },
      bar: { groupWidth: '75%' },
      isStacked: true,
    };

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.BarChart(document.getElementById('stack'));
    chart.draw(data, options);
  } 
});

  
