$(function() {
  if (!$('#calendar').length)
    return
  google.charts.load('current', {'packages':['corechart', 'calendar']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
       var dataTable = new google.visualization.DataTable();
       dataTable.addColumn({ type: 'date', id: 'Date' });
       dataTable.addColumn({ type: 'number', id: 'Sinavlar' });
       dataTable.addRows([
          [ new Date(2017, 4, 4), 2 ],
          [ new Date(2017, 4, 5), 1 ],
          [ new Date(2016, 11, 13), 1 ],
          [ new Date(2016, 11, 14), 2 ],
          // Many rows omitted for brevity.
        ]);

       var chart = new google.visualization.Calendar(document.getElementById('calendar'));

       var options = {
         title: "Sinav tarihleri",
         height: 350,
       };

       chart.draw(dataTable, options);
   }
})