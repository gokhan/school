$(function() {
  if (!$('#pie').length)
    return
  // Load the Visualization API and the corechart package.
  google.charts.load('current', {'packages':['corechart', 'calendar']});

  // Set a callback to run when the Google Visualization API is loaded.
  google.charts.setOnLoadCallback(drawChart);

  // Callback that creates and populates a data table,
  // instantiates the pie chart, passes in the data and
  // draws it.
  function drawChart() {

    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Sonuclar');
    data.addColumn('number', 'Ogrenci');
    data.addRows([
      ['Bitiren', 20],
      ['Yapmayan', 10],
      ['Gec yapan', 4],
    ]);

    // Set chart options
    var options = {'title':'23 Eylul Matematik teslim sayilari',
                   'width':900,
                   'height':700,
                   legend: { position: 'right', alignment: 'center' },
                   'is3D': true};
// var options = {
//   'legend':'left',
//   'title':'My Big Pie Chart',
//   'is3D':true,
//   'width':400,
//   'height':300
//}
    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.PieChart(document.getElementById('pie'));
    chart.draw(data, options);
  } 
});