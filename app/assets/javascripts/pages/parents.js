app.Parents = function() {
  this._input = $('#parents-search-txt');
  this._initAutocomplete();
};

app.Parents.prototype = {
  _initAutocomplete: function() {
    this._input
      .autocomplete({
        source: '/parents/search',
        appendTo: '#parents-search-results',
        select: $.proxy(this._select, this)
      })
      .autocomplete('instance')._renderItem = $.proxy(this._render, this);
  },

  _render: function(ul, item) {
    var markup = [
      '<span class="title">' + item.name + '</span>',
      '<span class="author">' + item.email + '</span>',
    ];
    return $('<li>')
      .append(markup.join(''))
      .appendTo(ul);
  },

  _select: function(e, ui) {
    this._input.val(ui.item.id + ' - ' + ui.item.name);
    return false;
  }
};