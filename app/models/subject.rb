# == Schema Information
#
# Table name: subjects
#
#  id          :integer          not null, primary key
#  unit        :string
#  name        :string
#  description :string
#  lesson_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_subjects_on_lesson_id  (lesson_id)
#

class Subject < ActiveRecord::Base
  belongs_to :lesson
  has_many :course_subjects, dependent: :delete_all
  has_many :courses, through: :course_subjects
end
