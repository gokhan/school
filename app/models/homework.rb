# == Schema Information
#
# Table name: homeworks
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  course_id   :integer
#  user_id     :integer
#  starts_at   :datetime
#  ends_at     :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_homeworks_on_course_id  (course_id)
#  index_homeworks_on_user_id    (user_id)
#

class Homework < ActiveRecord::Base
  belongs_to :course
  belongs_to :user
  has_many :homework_questions, dependent: :destroy
  has_many :questions, through: :homework_questions

  def doable?
    ends_at >= DateTime.now
  end

  def doing
    Take.where(homework:self)
  end
end
