# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  text       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  correct_id :integer
#
# Indexes
#
#  index_questions_on_user_id  (user_id)
#

class Question < ActiveRecord::Base
  acts_as_taggable
  
  has_many :choices, class_name: 'Option'
  has_one :correct, ->(q) { where(id: q.correct_id) }, class_name: 'Option'
  
  belongs_to :user
  has_many :homework_questions, dependent: :delete_all
  has_many :homeworks, through: :homework_questions

  # image
  has_one :image, as: :imageable, required: false

  def avatar
    image || Image.tagged_with("default,question", on: :image).first || Image.first
  end
  
end
