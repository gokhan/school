# == Schema Information
#
# Table name: course_subjects
#
#  id         :integer          not null, primary key
#  course_id  :integer
#  subject_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_course_subjects_on_course_id   (course_id)
#  index_course_subjects_on_subject_id  (subject_id)
#

class CourseSubject < ActiveRecord::Base
  belongs_to :course
  belongs_to :subject
end
