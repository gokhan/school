# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string
#  email      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  role_id    :integer
#
# Indexes
#
#  index_users_on_role_id  (role_id)
#

class User < ActiveRecord::Base
  has_many :questions, dependent: :nullify
  belongs_to :role

  # student/parent relations
  has_many :user_parents, dependent: :delete_all
  has_many :parents, through: :user_parents, source: :parent

  has_many :user_children, class_name: 'UserParent', foreign_key: :parent_id, dependent: :delete_all
  has_many :children, through: :user_children, source: :user

  # rooms
  has_many :user_rooms, dependent: :delete_all
  has_many :rooms, through: :user_rooms
  has_many :messages, through: :rooms
  # image
  has_one :image, as: :imageable, required: false
  # preference
  has_one :preference

  acts_as_messageable

  def courses
    case role.key.to_sym
    when :teacher
      Course.where(user: self)
    when :student
      rooms.map{|room| room.courses}.uniq
    end
  end

  # define admins, teachers, students, parents scopes 
  # define is_? methods
  %w(admin teacher student parent).each do |role|

    define_method "is_#{role}?" do
      role == self.role.key
    end
  end
  scope :teachers, -> { joins(:role).where("roles.key='teacher'") }
  scope :students, -> { joins(:role).where("roles.key='student'") }
  scope :parentz, -> { joins(:role).where("roles.key='parent'") }
    

  def avatar
    image || Image.tagged_with("default", on: :image).first || Image.first
  end
end
