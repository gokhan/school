# == Schema Information
#
# Table name: user_parents
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  parent_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_user_parents_on_user_id  (user_id)
#

class UserParent < ActiveRecord::Base
  belongs_to :user
  belongs_to :parent, class_name: 'User'
end
