# == Schema Information
#
# Table name: courses
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  room_id     :integer
#  lesson_id   :integer
#
# Indexes
#
#  index_courses_on_lesson_id  (lesson_id)
#  index_courses_on_room_id    (room_id)
#  index_courses_on_user_id    (user_id)
#

class Course < ActiveRecord::Base
  belongs_to :user # as teacher
  has_many :homeworks, dependent: :destroy
  # rooms
  belongs_to :room

  # subjects
  belongs_to :lesson

  has_many :course_subjects, dependent: :delete_all
  has_many :subjects, through: :course_subjects

  # image
  has_one :image, as: :imageable, required: false

  # videos
  has_many :videos, as: :videoable

  def students
    room.users
  end
  
  def avatar
    image || Image.tagged_with("default,course", on: :image).first || Image.first
  end
end
