# == Schema Information
#
# Table name: hours
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  from       :string           not null
#  to         :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Hour < ActiveRecord::Base
end
