# == Schema Information
#
# Table name: schedules
#
#  id         :integer          not null, primary key
#  day        :integer
#  hour_id    :integer
#  room_id    :integer
#  course_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_schedules_on_course_id  (course_id)
#  index_schedules_on_hour_id    (hour_id)
#  index_schedules_on_room_id    (room_id)
#

class Schedule < ActiveRecord::Base
  DAYS = (1..5).to_a
  belongs_to :hour
  belongs_to :room
  belongs_to :course
end
