# == Schema Information
#
# Table name: rooms
#
#  id         :integer          not null, primary key
#  name       :string
#  active     :boolean          default(TRUE)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Room < ActiveRecord::Base
  has_many :user_rooms, dependent: :delete_all
  has_many :users, through: :user_rooms, class: ::User

  has_many :courses
  has_many :homeworks, through: :courses

  has_many :announcement_rooms, dependent: :delete_all
  has_many :announcements, through: :announcement_rooms

  has_many :comments, as: :commentable
  has_many :schedules
end
