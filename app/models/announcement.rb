# == Schema Information
#
# Table name: announcements
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  text       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_announcements_on_user_id  (user_id)
#

class Announcement < ActiveRecord::Base
  belongs_to :user

  has_many :announcement_rooms, dependent: :delete_all
  has_many :rooms, through: :announcement_rooms
end
