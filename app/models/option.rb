# == Schema Information
#
# Table name: options
#
#  id          :integer          not null, primary key
#  question_id :integer
#  text        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_options_on_question_id  (question_id)
#

class Option < ActiveRecord::Base
  belongs_to :question
  # image
  has_one :image, as: :imageable, required: false

  def avatar
    image || Image.tagged_with("default,question", on: :image).first || Image.first
  end
  
end
