# == Schema Information
#
# Table name: announcement_rooms
#
#  id              :integer          not null, primary key
#  room_id         :integer
#  announcement_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_announcement_rooms_on_announcement_id  (announcement_id)
#  index_announcement_rooms_on_room_id          (room_id)
#

class AnnouncementRoom < ActiveRecord::Base
  belongs_to :room
  belongs_to :announcement
end
