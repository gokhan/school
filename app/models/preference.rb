# == Schema Information
#
# Table name: preferences
#
#  id      :integer          not null, primary key
#  user_id :integer
#  data    :jsonb            default({}), not null
#
# Indexes
#
#  index_preferences_on_data     (data)
#  index_preferences_on_user_id  (user_id)
#

class Preference < ActiveRecord::Base
  belongs_to :user
  include Storext.model

  # You can define attributes on the :data hstore column like this:
  store_attributes :data do
    homework_notification_by_phone Boolean, default: false
    homework_notification_by_email Boolean, default: false
    announcement_notification_by_phone Boolean, default: false
    announcement_notification_by_email Boolean, default: false
  end
end
