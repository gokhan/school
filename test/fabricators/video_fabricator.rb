# == Schema Information
#
# Table name: videos
#
#  id             :integer          not null, primary key
#  title          :string
#  link           :string
#  description    :text
#  publish_at     :datetime
#  videoable_id   :integer
#  videoable_type :string
#  user_id        :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_videos_on_user_id                          (user_id)
#  index_videos_on_videoable_type_and_videoable_id  (videoable_type,videoable_id)
#

Fabricator(:video) do
  title       "MyString"
  link        "MyString"
  description "MyText"
  publish_at  "2017-01-14 08:58:24"
  videoable   nil
  user        nil
end
