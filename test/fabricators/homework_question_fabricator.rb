# == Schema Information
#
# Table name: homework_questions
#
#  id          :integer          not null, primary key
#  homework_id :integer
#  question_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_homework_questions_on_homework_id  (homework_id)
#  index_homework_questions_on_question_id  (question_id)
#

Fabricator(:homework_question) do
  homework nil
  question nil
end
