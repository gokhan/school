# == Schema Information
#
# Table name: options
#
#  id          :integer          not null, primary key
#  question_id :integer
#  text        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_options_on_question_id  (question_id)
#

Fabricator(:option) do
  question nil
  text     "MyString"
end
