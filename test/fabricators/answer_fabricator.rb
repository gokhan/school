# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  take_id     :integer
#  option_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  question_id :integer
#
# Indexes
#
#  index_answers_on_option_id    (option_id)
#  index_answers_on_question_id  (question_id)
#  index_answers_on_take_id      (take_id)
#  index_answers_on_user_id      (user_id)
#

Fabricator(:answer) do
  user   nil
  take   nil
  option nil
end
