# == Schema Information
#
# Table name: takes
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  homework_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  right       :integer          default(0), not null
#  wrong       :integer          default(0), not null
#  blank       :integer          default(0), not null
#
# Indexes
#
#  index_takes_on_homework_id  (homework_id)
#  index_takes_on_user_id      (user_id)
#

Fabricator(:take) do
  user     nil
  homework nil
end
