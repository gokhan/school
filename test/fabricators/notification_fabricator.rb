# == Schema Information
#
# Table name: notifications
#
#  id              :integer          not null, primary key
#  sourceable_id   :integer
#  sourceable_type :string
#  action          :string
#  notified        :boolean          default(FALSE)
#  boolean         :boolean          default(FALSE)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_notifications_on_sourceable_type_and_sourceable_id  (sourceable_type,sourceable_id)
#

Fabricator(:notification) do
  sourceable nil
  action     "MyString"
  notified   false
end
