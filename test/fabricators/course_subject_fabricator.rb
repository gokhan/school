# == Schema Information
#
# Table name: course_subjects
#
#  id         :integer          not null, primary key
#  course_id  :integer
#  subject_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_course_subjects_on_course_id   (course_id)
#  index_course_subjects_on_subject_id  (subject_id)
#

Fabricator(:course_subject) do
  course  nil
  subject nil
end
