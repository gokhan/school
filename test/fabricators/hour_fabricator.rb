# == Schema Information
#
# Table name: hours
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  from       :string           not null
#  to         :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

Fabricator(:hour) do
  name "MyString"
  from "2017-01-18 12:32:53"
  to   "2017-01-18 12:32:53"
end
