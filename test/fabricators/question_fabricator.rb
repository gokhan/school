# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  text       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  correct_id :integer
#
# Indexes
#
#  index_questions_on_user_id  (user_id)
#

Fabricator(:question) do
  text "MyString"
end
