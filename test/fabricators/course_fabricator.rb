# == Schema Information
#
# Table name: courses
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  user_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  room_id     :integer
#  lesson_id   :integer
#
# Indexes
#
#  index_courses_on_lesson_id  (lesson_id)
#  index_courses_on_room_id    (room_id)
#  index_courses_on_user_id    (user_id)
#

Fabricator(:course) do
  name        "MyString"
  description "MyText"
end
