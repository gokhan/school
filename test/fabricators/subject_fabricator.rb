# == Schema Information
#
# Table name: subjects
#
#  id          :integer          not null, primary key
#  unit        :string
#  name        :string
#  description :string
#  lesson_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_subjects_on_lesson_id  (lesson_id)
#

Fabricator(:subject) do
  unit        "MyString"
  name        "MyString"
  description "MyString"
  lesson      nil
end
