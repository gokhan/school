# == Schema Information
#
# Table name: homeworks
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  course_id   :integer
#  user_id     :integer
#  starts_at   :datetime
#  ends_at     :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_homeworks_on_course_id  (course_id)
#  index_homeworks_on_user_id    (user_id)
#

Fabricator(:homework) do
  name        "MyString"
  description "MyText"
  course      nil
  user        nil
end
