require "test_helper"

describe HomeworkQuestion do
  let(:homework_question) { HomeworkQuestion.new }

  it "must be valid" do
    value(homework_question).must_be :valid?
  end
end
