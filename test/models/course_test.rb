# == Schema Information
#
# Table name: courses
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require "test_helper"

describe Course do
  let(:course) { Course.new }

  it "must be valid" do
    value(course).must_be :valid?
  end
end
