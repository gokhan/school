require "test_helper"

describe Option do
  let(:option) { Option.new }

  it "must be valid" do
    value(option).must_be :valid?
  end
end
