class DropHasManyToRooms < ActiveRecord::Migration
  def change
    drop_table :course_rooms

    add_reference :courses, :room, index: true

    Course.update_all(room_id: Room.first.id) if Room.any?
  end
end
