class RenameMessageToAnnouncement < ActiveRecord::Migration
  def change
    rename_table :messages, :announcements
    rename_table :message_rooms, :announcement_rooms

    rename_column :announcement_rooms, :message_id, :announcement_id
  end
end
