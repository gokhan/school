class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :sourceable, polymorphic: true, index: true
      t.string :action
      t.boolean :notified, :boolean, default: false

      t.timestamps null: false
    end
  end
end
