class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :unit
      t.string :name
      t.string :description
      t.references :lesson, index: true

      t.timestamps null: false
    end
    add_foreign_key :subjects, :lessons
  end
end
