class CreateMessageRooms < ActiveRecord::Migration
  def change
    create_table :message_rooms do |t|
      t.references :room, index: true
      t.references :message, index: true

      t.timestamps null: false
    end
    add_foreign_key :message_rooms, :rooms
    add_foreign_key :message_rooms, :messages
  end
end
