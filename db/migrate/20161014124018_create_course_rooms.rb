class CreateCourseRooms < ActiveRecord::Migration
  def change
    create_table :course_rooms do |t|
      t.references :course, index: true
      t.references :room, index: true

      t.timestamps null: false
    end
    add_foreign_key :course_rooms, :courses
    add_foreign_key :course_rooms, :rooms
  end
end
