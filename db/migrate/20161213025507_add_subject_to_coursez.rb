class AddSubjectToCoursez < ActiveRecord::Migration
  def change
    add_reference :courses, :subject, index: true
    add_foreign_key :courses, :subjects
  end
end
