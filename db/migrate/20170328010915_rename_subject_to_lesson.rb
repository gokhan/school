class RenameSubjectToLesson < ActiveRecord::Migration
  def change
  	rename_table :subjects, :lessons
  	rename_column :courses, :subject_id, :lesson_id
  end
end
