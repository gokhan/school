class AddRightWrongEmptyToTakes < ActiveRecord::Migration
  def change
    add_column :takes, :right, :integer, default: 0, null: false
    add_column :takes, :wrong, :integer, default: 0, null: false
    add_column :takes, :blank, :integer, default: 0, null: false
  end
end
