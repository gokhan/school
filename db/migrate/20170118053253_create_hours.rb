class CreateHours < ActiveRecord::Migration
  def change
    create_table :hours do |t|
      t.string :name, null: false
      t.string :from, null: false
      t.string :to, null: false

      t.timestamps null: false
    end
  end
end
