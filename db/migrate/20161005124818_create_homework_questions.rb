class CreateHomeworkQuestions < ActiveRecord::Migration
  def change
    create_table :homework_questions do |t|
      t.references :homework, index: true
      t.references :question, index: true

      t.timestamps null: false
    end
    add_foreign_key :homework_questions, :homeworks
    add_foreign_key :homework_questions, :questions
  end
end
