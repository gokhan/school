class AddChoiceToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :correct_id, :integer, index: true
  end
end
