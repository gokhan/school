class CreateHomeworks < ActiveRecord::Migration
  def change
    create_table :homeworks do |t|
      t.string :name
      t.text :description
      t.references :course, index: true
      t.references :user, index: true
      t.datetime :starts_at 
      t.datetime :ends_at 

      t.timestamps null: false
    end
    add_foreign_key :homeworks, :courses
    add_foreign_key :homeworks, :users
  end
end
