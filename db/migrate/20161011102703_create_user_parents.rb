class CreateUserParents < ActiveRecord::Migration
  def change
    create_table :user_parents do |t|
      t.references :user, index: true
      t.integer :parent_id

      t.timestamps null: false
    end
    add_foreign_key :user_parents, :users
  end
end
