class CreateTakes < ActiveRecord::Migration
  def change
    create_table :takes do |t|
      t.references :user, index: true
      t.references :homework, index: true

      t.timestamps null: false
    end
    add_foreign_key :takes, :users
    add_foreign_key :takes, :homeworks
  end
end
