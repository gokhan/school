class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.integer :day
      t.references :hour, index: true
      t.references :room, index: true
      t.references :course, index: true

      t.timestamps null: false
    end
    add_foreign_key :schedules, :hours
    add_foreign_key :schedules, :rooms
    add_foreign_key :schedules, :courses
  end
end
