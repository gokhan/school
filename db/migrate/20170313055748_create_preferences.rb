class CreatePreferences < ActiveRecord::Migration
  def change
  	create_table :preferences do |t|
			t.references :user, index: true
			t.jsonb :data, null: false, default: '{}'
		end

		add_index :preferences, :data, using: :gin  	
  end
end
