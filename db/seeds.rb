yonetici = Role.create name: "yönetici", key: 'admin'
ogretmen = Role.create name: "öğretmen", key: 'teacher'
ogrenci = Role.create name: "öğrenci", key: 'student'
veli = Role.create name: "veli", key: 'parent'

mudur  = User.create name:'Yönetici', email:'yonetici@example.com', role: yonetici
mahmut = User.create name:'Mahmut Hoca', email:'ogretmen@example.com', role: ogretmen
tarik  = User.create name:'Tarik Akan', email:'ogretmen@example.com', role: ogrenci
veli = User.create name: 'Mehmet Gungor', email:'mehmet@gungor.com', role: veli
matematik = Course.create name:'Metamatik', 
                          description:'Bu derste istatistik yogunluklu calisicagiz',
                          teacher: mahmut
ingilizce = Course.create name:'Ingilizce', 
                          description:'Ingilizce Gramer',
                          teacher: mahmut

room = Room.create name: '4A'

image = Image.create(remote_data_url: 'https://lh3.googleusercontent.com/I0nx6CNiA9Br45CykHDEu6cGnUbZ4klaxbxYvIyYTjXJJHCE3tjDPisrmW2UDlsddr1ZCrZ74gDsZa6dFIXLUQ=s0')
image.set_tag_list_on(:image, 'default,student,male')
image.save

image = Image.create(remote_data_url: 'http://www.freeiconspng.com/uploads/-avatar-people-person-profile-user-women-icon--icon-search-engine-23.png')
image.set_tag_list_on(:image, 'default,student,female')
image.save

image = Image.create(remote_data_url: 'https://cdn1.iconfinder.com/data/icons/rcons-user-action/512/users_woman-512.png')
image.set_tag_list_on(:image, 'default,parent')
image.save

image = Image.create(remote_data_url: 'https://cdn3.iconfinder.com/data/icons/rcons-user-action/32/girl-512.png')
image.set_tag_list_on(:image, 'default,teacher')
image.save

image = Image.create(remote_data_url: 'http://www.freeiconspng.com/uploads/book-icon--icon-search-engine-6.png')
image.set_tag_list_on(:image, 'default,course')
image.save

image = Image.create(remote_data_url: 'http://www.freeiconspng.com/uploads/ask-icon-19.png')
image.set_tag_list_on(:image, 'default,question')
image.save

['Türkçe', 'Matematik', 'Fen Bilimleri', 'İnkılap Tarihi ve Atatürkçülük', 
  'Din Kültürü ve Ahlak Bilgisi', 'Din Kültürü ve Ahlak Bilgisi / Musevi Okulları', 
  'Din Kültürü ve Ahlak Bilgisi / Ermeni Okulları', 'İngilizce', 'Almanca', 'Fransızca', 'İtalyanca',].each do |name|
  Lesson.create name: name
end

Lesson.all.each do |lesson|  
  file_name = "#{Rails.root}/db/csv/#{lesson.name}.csv"
  if File.exist?(file_name)
    CSV.foreach(file_name, {:headers => true}) do |row|
      Subject.create unit: row[0], name: row[1], description: row[2], lesson: lesson
    end
  end
end

Hour.create([
  {name: '1. Ders', from: "9:00", to: "9:40"},
  {name: '2. Ders', from: "9:55", to: "10:35"},
  {name: '3. Ders', from: "10:45", to: "11:25"},
  {name: '4. Ders', from: "11:35", to: "12:15"},
  {name: '5. Ders', from: "13:05", to: "13:45"},
  {name: '6. Ders', from: "13:55", to: "14:35"},
  {name: '7. Ders', from: "14:45", to: "15:25"},
])  



